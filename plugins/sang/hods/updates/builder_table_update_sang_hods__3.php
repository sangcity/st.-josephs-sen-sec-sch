<?php namespace Sang\Hods\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSangHods3 extends Migration
{
    public function up()
    {
        Schema::table('sang_hods_', function($table)
        {
            $table->string('teachers')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('sang_hods_', function($table)
        {
            $table->dropColumn('teachers');
        });
    }
}
