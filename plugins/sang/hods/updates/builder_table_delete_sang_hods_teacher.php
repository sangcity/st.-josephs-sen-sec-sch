<?php namespace Sang\Hods\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteSangHodsTeacher extends Migration
{
    public function up()
    {
        Schema::dropIfExists('sang_hods_teacher');
    }
    
    public function down()
    {
        Schema::create('sang_hods_teacher', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 191);
            $table->text('description');
            $table->string('slug', 191);
            $table->string('subject', 191);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
