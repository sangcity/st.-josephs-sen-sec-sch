<?php namespace Sang\Hods\Models;

use Model;

/**
 * Model
 */
class HODS extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'sang_hods_';


    protected $jsonable = ['teachers'];


    /**Relations */
    public $attachOne = [
        'headofdept_img' =>'System\Models\File'
    ];

    public $attachMany = [
        'teachers_img' => 'System\Models\File'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
