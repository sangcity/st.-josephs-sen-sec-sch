<?php namespace Sang\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSangEvents extends Migration
{
    public function up()
    {
        Schema::create('sang_events_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('slug');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->dateTime('date');
            $table->string('location');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('sang_events_');
    }
}
