<?php namespace Sang\Events\Models;

use Model;

/**
 * Model
 */
class Events extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'sang_events_';

    /** Relations */

    public $attachOne = [
        'event_img' => 'System\Models\File'
    ];

    public $attachMany = [
        'event_gallery' => 'System\Models\File'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
