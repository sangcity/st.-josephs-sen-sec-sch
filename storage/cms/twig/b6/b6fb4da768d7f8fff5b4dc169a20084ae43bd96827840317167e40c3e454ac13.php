<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/partials/navigation.htm */
class __TwigTemplate_f928462504939b5052a12f50a462793c5f6308e2f41bfeab4cc8aeb5affd997c extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"fh5co-loader\"></div>
\t
\t<nav class=\"fh5co-nav\" role=\"navigation\">
\t\t<div class=\"top\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-12 text-right\">
\t\t\t\t\t\t<p class=\"num\">Call: +220 ‭422-7709</p>
\t\t\t\t\t\t<ul class=\"fh5co-social\">
\t\t\t\t\t\t\t<li><a
\t\t\t\t\t\t\t\t\thref=\"https://www.facebook.com/Saint-Josephs-Senior-Secondary-School-2481936115172108/?modal=admin_todo_tour\" target=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-facebook\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"https://www.youtube.com/channel/UC3nddtNh66EKLt_gLxKsb6w\" target=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-youtube\"></i></a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"top-menu\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-1 logo-top\">
\t\t\t\t\t\t<div id=\"fh5co-logo\"><a href=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\"><span><img class=\"sch-logo\" src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/schLogo.png");
        echo "\"
\t\t\t\t\t\t\t\t\t\talt=\"work\"></span></a></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-11 text-right menu-1\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li class=\"";
        // line 29
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 29), "id", [], "any", false, false, false, 29) == "home")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">Home</a></li>
\t\t\t\t\t\t\t<li class=\"";
        // line 30
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 30), "id", [], "any", false, false, false, 30) == "about")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about");
        echo "\">About</a></li>
\t\t\t\t\t\t\t<li class=\"";
        // line 31
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 31), "id", [], "any", false, false, false, 31) == "events")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("events");
        echo "\">Events</a></li>
\t\t\t\t\t\t\t<li class=\"";
        // line 32
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 32), "id", [], "any", false, false, false, 32) == "blog")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\">Blog</a></li>
\t\t\t\t\t\t\t<li class=\"";
        // line 33
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33) == "gallery")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("gallery");
        echo "\">Gallery</a></li>
\t\t\t\t\t\t\t<!-- <li class=\"has-dropdown\">
\t\t\t\t\t\t\t\t<a href=\"blog.html\">Blog</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Web Design</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">eCommerce</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Branding</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">API</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li> -->
\t\t\t\t\t\t\t<li><a href=\"";
        // line 43
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">Contact</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</nav>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/partials/navigation.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 43,  102 => 33,  94 => 32,  86 => 31,  78 => 30,  70 => 29,  60 => 24,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"fh5co-loader\"></div>
\t
\t<nav class=\"fh5co-nav\" role=\"navigation\">
\t\t<div class=\"top\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-12 text-right\">
\t\t\t\t\t\t<p class=\"num\">Call: +220 ‭422-7709</p>
\t\t\t\t\t\t<ul class=\"fh5co-social\">
\t\t\t\t\t\t\t<li><a
\t\t\t\t\t\t\t\t\thref=\"https://www.facebook.com/Saint-Josephs-Senior-Secondary-School-2481936115172108/?modal=admin_todo_tour\" target=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-facebook\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"https://www.youtube.com/channel/UC3nddtNh66EKLt_gLxKsb6w\" target=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-youtube\"></i></a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"top-menu\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-1 logo-top\">
\t\t\t\t\t\t<div id=\"fh5co-logo\"><a href=\"{{ 'home'|page }}\"><span><img class=\"sch-logo\" src=\"{{ 'assets/images/schLogo.png'|theme }}\"
\t\t\t\t\t\t\t\t\t\talt=\"work\"></span></a></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-11 text-right menu-1\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li class=\"{% if this.page.id == 'home' %}active{% endif %}\"><a href=\"{{ 'home'|page }}\">Home</a></li>
\t\t\t\t\t\t\t<li class=\"{% if this.page.id == 'about' %}active{% endif %}\"><a href=\"{{ 'about'|page }}\">About</a></li>
\t\t\t\t\t\t\t<li class=\"{% if this.page.id == 'events' %}active{% endif %}\"><a href=\"{{ 'events'|page }}\">Events</a></li>
\t\t\t\t\t\t\t<li class=\"{% if this.page.id == 'blog' %}active{% endif %}\"><a href=\"{{ 'blog'|page }}\">Blog</a></li>
\t\t\t\t\t\t\t<li class=\"{% if this.page.id == 'gallery' %}active{% endif %}\"><a href=\"{{ 'gallery'|page }}\">Gallery</a></li>
\t\t\t\t\t\t\t<!-- <li class=\"has-dropdown\">
\t\t\t\t\t\t\t\t<a href=\"blog.html\">Blog</a>
\t\t\t\t\t\t\t\t<ul class=\"dropdown\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Web Design</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">eCommerce</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Branding</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">API</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</li> -->
\t\t\t\t\t\t\t<li><a href=\"{{ 'contact'|page }}\">Contact</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</nav>", "/Users/user/Sites/stjosephsss/themes/st-josephs/partials/navigation.htm", "");
    }
}
