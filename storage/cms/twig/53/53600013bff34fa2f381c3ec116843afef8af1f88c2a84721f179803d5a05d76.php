<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/layouts/default-layout.htm */
class __TwigTemplate_b526c49bdaaa0762414b1de9e20633a0f302165626ba85c6ae83f6b7c04fbe09 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>
\t<head>
\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<title>SJSSS</title>
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta name=\"description\" content=\"Free HTML5 Website Template by freehtml5.co\" />
\t<meta name=\"keywords\" content=\"free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive\" />
\t<meta name=\"author\" content=\"freehtml5.co\" />
\t
  \t<!-- Facebook and Twitter integration -->
\t<meta property=\"og:title\" content=\"\"/>
\t<meta property=\"og:image\" content=\"\"/>
\t<meta property=\"og:url\" content=\"\"/>
\t<meta property=\"og:site_name\" content=\"\"/>
\t<meta property=\"og:description\" content=\"\"/>
\t<meta name=\"twitter:title\" content=\"\" />
\t<meta name=\"twitter:image\" content=\"\" />
\t<meta name=\"twitter:url\" content=\"\" />
\t<meta name=\"twitter:card\" content=\"\" />

\t<link href=\"https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800\" rel=\"stylesheet\">
\t
\t<!-- Animate.css -->
\t<link rel=\"stylesheet\" href=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/animate.css");
        echo "\">
\t<!-- Icomoon Icon Fonts-->
\t<link rel=\"stylesheet\" href=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/icomoon.css");
        echo "\">
\t<!-- Bootstrap  -->
\t<link rel=\"stylesheet\" href=\"";
        // line 30
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.css");
        echo "\">

\t<!-- Magnific Popup -->
\t<link rel=\"stylesheet\" href=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/magnific-popup.css");
        echo "\">

\t<!-- Owl Carousel  -->
\t<link rel=\"stylesheet\" href=\"";
        // line 36
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/owl.carousel.min.css");
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 37
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/owl.theme.default.min.css");
        echo "\">

\t<!-- Theme style  -->
\t<link rel=\"stylesheet\" href=\"";
        // line 40
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/style.css");
        echo "\">

\t<!-- Modernizr JS -->
\t<script src=\"";
        // line 43
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/modernizr-2.6.2.min.js");
        echo "\"></script>
\t<!-- FOR IE9 below -->
\t<!--[if lt IE 9]>
\t<script src=\"js/respond.min.js\"></script>
\t
\t<![endif]-->

\t   <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 50
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/lightgallery.css");
        echo "\" />

\t";
        // line 52
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 53
        echo "\t";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 54
        echo "\t

\t</head>
\t<body>

\t";
        // line 59
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("navigation"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 60
        echo "\t
\t";
        // line 61
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 62
        echo "
\t";
        // line 63
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 64
        echo "


\t
\t
\t<div class=\"gototop js-top\">
\t\t<a href=\"#\" class=\"js-gotop\"><i class=\"icon-arrow-up\"></i></a>
\t</div>
\t
\t<!-- jQuery -->
\t<script src=\"";
        // line 74
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery.min.js");
        echo "\"></script>
\t<!-- jQuery Easing -->
\t<script src=\"";
        // line 76
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery.easing.1.3.js");
        echo "\"></script>
\t<!-- Bootstrap -->
\t<script src=\"";
        // line 78
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/bootstrap.min.js");
        echo "\"></script>
\t<!-- Waypoints -->
\t<script src=\"";
        // line 80
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery.waypoints.min.js");
        echo "\"></script>
\t<!-- Stellar Parallax -->
\t<script src=\"";
        // line 82
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery.stellar.min.js");
        echo "\"></script>
\t<!-- Carousel -->
\t<script src=\"";
        // line 84
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/owl.carousel.min.js");
        echo "\"></script>
\t<!-- countTo -->
\t<script src=\"";
        // line 86
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery.countTo.js");
        echo "\"></script>
\t<!-- Magnific Popup -->
\t<script src=\"";
        // line 88
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery.magnific-popup.min.js");
        echo "\"></script>
\t<script src=\"";
        // line 89
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/magnific-popup-options.js");
        echo "\"></script>
\t<!-- Main -->
\t<script src=\"";
        // line 91
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/main.js");
        echo "\"></script>



\t   <!-- jQuery version must be >= 1.8.0; -->
\t   <script src=\"jquery.min.js\"></script>
\t   <script src=\"js/lightgallery.min.js\"></script>

\t   <!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
\t   <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js\"></script>

\t   <!-- lightgallery plugins -->
\t   <script src=\"";
        // line 103
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/lg-thumbnail.min.js");
        echo "\"></script>
\t   <script src=\"";
        // line 104
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/lg-fullscreen.min.js");
        echo "\"></script>
\t";
        // line 105
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 106
        echo "\t</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/layouts/default-layout.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 106,  224 => 105,  220 => 104,  216 => 103,  201 => 91,  196 => 89,  192 => 88,  187 => 86,  182 => 84,  177 => 82,  172 => 80,  167 => 78,  162 => 76,  157 => 74,  145 => 64,  141 => 63,  138 => 62,  136 => 61,  133 => 60,  129 => 59,  122 => 54,  118 => 53,  115 => 52,  110 => 50,  100 => 43,  94 => 40,  88 => 37,  84 => 36,  78 => 33,  72 => 30,  67 => 28,  62 => 26,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE HTML>
<html>
\t<head>
\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<title>SJSSS</title>
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta name=\"description\" content=\"Free HTML5 Website Template by freehtml5.co\" />
\t<meta name=\"keywords\" content=\"free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive\" />
\t<meta name=\"author\" content=\"freehtml5.co\" />
\t
  \t<!-- Facebook and Twitter integration -->
\t<meta property=\"og:title\" content=\"\"/>
\t<meta property=\"og:image\" content=\"\"/>
\t<meta property=\"og:url\" content=\"\"/>
\t<meta property=\"og:site_name\" content=\"\"/>
\t<meta property=\"og:description\" content=\"\"/>
\t<meta name=\"twitter:title\" content=\"\" />
\t<meta name=\"twitter:image\" content=\"\" />
\t<meta name=\"twitter:url\" content=\"\" />
\t<meta name=\"twitter:card\" content=\"\" />

\t<link href=\"https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800\" rel=\"stylesheet\">
\t
\t<!-- Animate.css -->
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/animate.css'|theme }}\">
\t<!-- Icomoon Icon Fonts-->
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/icomoon.css'|theme }}\">
\t<!-- Bootstrap  -->
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/bootstrap.css'|theme }}\">

\t<!-- Magnific Popup -->
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/magnific-popup.css'|theme }}\">

\t<!-- Owl Carousel  -->
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/owl.carousel.min.css'|theme }}\">
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/owl.theme.default.min.css'|theme }}\">

\t<!-- Theme style  -->
\t<link rel=\"stylesheet\" href=\"{{ 'assets/css/style.css'|theme }}\">

\t<!-- Modernizr JS -->
\t<script src=\"{{ 'assets/js/modernizr-2.6.2.min.js'|theme }}\"></script>
\t<!-- FOR IE9 below -->
\t<!--[if lt IE 9]>
\t<script src=\"js/respond.min.js\"></script>
\t
\t<![endif]-->

\t   <link type=\"text/css\" rel=\"stylesheet\" href=\"{{ 'assets/css/lightgallery.css'|theme }}\" />

\t{% styles %}
\t{% scripts %}
\t

\t</head>
\t<body>

\t{% partial 'navigation' %}
\t
\t{% page %}

\t{% partial 'footer' %}



\t
\t
\t<div class=\"gototop js-top\">
\t\t<a href=\"#\" class=\"js-gotop\"><i class=\"icon-arrow-up\"></i></a>
\t</div>
\t
\t<!-- jQuery -->
\t<script src=\"{{ 'assets/js/jquery.min.js'|theme}}\"></script>
\t<!-- jQuery Easing -->
\t<script src=\"{{ 'assets/js/jquery.easing.1.3.js'|theme}}\"></script>
\t<!-- Bootstrap -->
\t<script src=\"{{ 'assets/js/bootstrap.min.js'|theme}}\"></script>
\t<!-- Waypoints -->
\t<script src=\"{{ 'assets/js/jquery.waypoints.min.js'|theme}}\"></script>
\t<!-- Stellar Parallax -->
\t<script src=\"{{ 'assets/js/jquery.stellar.min.js'|theme}}\"></script>
\t<!-- Carousel -->
\t<script src=\"{{ 'assets/js/owl.carousel.min.js'|theme}}\"></script>
\t<!-- countTo -->
\t<script src=\"{{ 'assets/js/jquery.countTo.js'|theme}}\"></script>
\t<!-- Magnific Popup -->
\t<script src=\"{{ 'assets/js/jquery.magnific-popup.min.js'|theme}}\"></script>
\t<script src=\"{{ 'assets/js/magnific-popup-options.js'|theme}}\"></script>
\t<!-- Main -->
\t<script src=\"{{ 'assets/js/main.js'|theme}}\"></script>



\t   <!-- jQuery version must be >= 1.8.0; -->
\t   <script src=\"jquery.min.js\"></script>
\t   <script src=\"js/lightgallery.min.js\"></script>

\t   <!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
\t   <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js\"></script>

\t   <!-- lightgallery plugins -->
\t   <script src=\"{{ 'assets/js/lg-thumbnail.min.js'|theme }}\"></script>
\t   <script src=\"{{ 'assets/js/lg-fullscreen.min.js'|theme }}\"></script>
\t{% framework extras %}
\t</body>
</html>", "/Users/user/Sites/stjosephsss/themes/st-josephs/layouts/default-layout.htm", "");
    }
}
