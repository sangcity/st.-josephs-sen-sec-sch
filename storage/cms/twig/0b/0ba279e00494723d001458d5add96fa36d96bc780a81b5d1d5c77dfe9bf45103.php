<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/about.htm */
class __TwigTemplate_aefeb0186925063a70b4eb4991cf8e45e899a4401aa5c083d796604da9d6004f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\" style=\"background-image:url(";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/picture.jpg");
        echo ");\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Know About SJSSS</h1>
\t\t\t\t\t\t\t<h2>We are an all-girl school. Thus we're proud of who we are as Josephina's.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>

\t<div id=\"fh5co-counter\" class=\"fh5co-counters\" style=\"background-color: #f2f2f2;\">
\t\t<div class=\"container\">
\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t<h2>Our School By Numbers</h2>
\t\t\t\t\t\t<p>The statistics are what make us keep going, thus constant improvement would be achieved.</P>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span id=\"icons\" class=\"icon-book\"></span>
\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">1:20</h2></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Students to Teachers Ratio</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span id=\"icons\" class=\"icon-trophy\"></span>
\t\t\t\t\t\t<spa>
\t\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">100%</h2></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Participation In Extra Currillum Activities</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span id=\"icons\" class=\"icon-graduation-cap\"></span>
\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">15</h2></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Graduate Teachers</span>
\t\t\t\t</div>\t
\t\t\t</div>

\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 text-center animate-box\">
\t\t\t\t\t\t\t<span id=\"icons\" class=\"icon-graduation-cap\"></span>
\t\t\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">95%</h2></span>
\t\t\t\t\t\t\t<span class=\"fh5co-counter-label\">Students Graduation Rate</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-6 text-center animate-box\">
\t\t\t\t\t\t\t\t<span id=\"icons\" class=\"icon-hand\"></span>
\t\t\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">15</h2></span>
\t\t\t\t\t\t\t\t<span class=\"fh5co-counter-label\">Enrolled In Vocational Stream</span>
\t\t\t\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"fh5co-explore\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2>About Us</h2>
\t\t\t\t\t<p>
\t\t\t\t\t    We have gone through so much for almost a century, browse through our history, know us more.
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"fh5co-explore fh5co-explore1\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-5 col-md-push-7 animate-box mt-60\">
\t\t\t\t\t\t<img class=\"img-responsive abt-img\" src=\"";
        // line 74
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/principal.png");
        echo "\" alt=\"work\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-7 col-md-pull-5 animate-box\">
\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t<h3>We Are The Great St. Josephs</h3>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tSince 1883, the Sisters of St. Joseph of Cluny have worked in The Gambia with devotion and dedication in the
\t\t\t\t\t\t\t\t\tfields of Education, Nursing and other social work for the promotion of the full development of the Gambian
\t\t\t\t\t\t\t\t\tpeople.
\t\t\t\t\t\t\t\t\tSt. Joseph’s Senior Secondary School (previously St. Joseph’s High School) was established in 1921 as a Roman
\t\t\t\t\t\t\t\t\tCatholic Mission School for Girls by the Congregation with an initial enrolment of forty-four students headed
\t\t\t\t\t\t\t\t\tby Mrs. Nellie Morris who handed over to Sr. Celeste Wall in 1923. The school’s policy was to instil self
\t\t\t\t\t\t\t\t\tdiscipline, inculcate moral values and to provide a model education for its students, Christians and Muslims
\t\t\t\t\t\t\t\t\talike; with basic reading, writing, arithmetic, music (introduced in 1926), home economics skills with a great
\t\t\t\t\t\t\t\t\temphasis on religious education. The Cluny Sisters at the time believed that education provided skills for
\t\t\t\t\t\t\t\t\tgirls to become good home managers and disciplined citizens within their communities.
\t\t\t\t\t\t\t\t\tOn the 10th of April 1940, Sr. Jarlath Hoade took over the mantle, the only principal to head both the
\t\t\t\t\t\t\t\t\tsecondary and primary schools concurrently.
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t
\t\t\t\t\t<!-- Button trigger modal -->
\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalScrollable\">
\t\t\t\t\t\tContinue Reading 
\t\t\t\t\t</button>

\t\t\t\t\t<!-- Modal -->
\t\t\t\t\t<div class=\"modal fade\" id=\"exampleModalScrollable\" tabindex=\"-1\" role=\"dialog\"
\t\t\t\t\t\taria-labelledby=\"exampleModalScrollableTitle\" aria-hidden=\"true\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">
\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalScrollableTitle\">All About Our History</h5>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\tThe period running up to and soon after independence in 1965, brought numerous reforms in the education
\t\t\t\t\t\t\t\t\tsystem
\t\t\t\t\t\t\t\t\tin the Gambia. Student Enrolment increased over the years as the demand for secondary education escalated
\t\t\t\t\t\t\t\t\tin the
\t\t\t\t\t\t\t\t\tcountry; St. Joseph’s grew to a two stream school in 1975, necessitating the move to its present location
\t\t\t\t\t\t\t\t\tat
\t\t\t\t\t\t\t\t\tCampama in 1976 through the leadership of Sr. Benigna Kearnet, who later handed over to Sr. Theresa
\t\t\t\t\t\t\t\t\tMundow. She
\t\t\t\t\t\t\t\t\twas succeeded by Mrs. Elsie Shyngle who passed the baton on to Sr. Leo. All these remarkable women,
\t\t\t\t\t\t\t\t\tcontributed
\t\t\t\t\t\t\t\t\tgreatly to the development of St. Joseph’s.

\t\t\t\t\t\t\t\t\tThe importance of education in the life of an individual cannot be overstated. Education shapes
\t\t\t\t\t\t\t\t\tindividuals in
\t\t\t\t\t\t\t\t\ttheir formative years; and imbues and inculcates them with morals and values that would govern all their
\t\t\t\t\t\t\t\t\tchoices
\t\t\t\t\t\t\t\t\tin life. This is the raison d’être of St. Joseph’s as embodied in our MISSION.
\t\t\t\t\t\t\t\t\tWith the ever increasing changes in the world and the rapid trends in development, it became even more
\t\t\t\t\t\t\t\t\timportant
\t\t\t\t\t\t\t\t\tto equip our girls with the prerequisite skills and knowledge that will help them fit in a global
\t\t\t\t\t\t\t\t\tvillage. The
\t\t\t\t\t\t\t\t\tAnne Marie Javouhey Libray built in the 80’s remains an important facility for our girls. At St. Joseph’s
\t\t\t\t\t\t\t\t\twe
\t\t\t\t\t\t\t\t\tstand always and forever committed to providing our students the best of opportunities so that each GIRL
\t\t\t\t\t\t\t\t\tgets a
\t\t\t\t\t\t\t\t\thead start in life, to reach the dizzy heights scaled by many of our distinguished alumnae.

\t\t\t\t\t\t\t\t\tI took over task of carrying this ethos forward on the 5th January 2011 from Sr. Catherine Jarra who took
\t\t\t\t\t\t\t\t\tover
\t\t\t\t\t\t\t\t\tin 2000 and who was preceded by Mrs. Vicky Ndure, principal from 1993. These two strong and dynamic women
\t\t\t\t\t\t\t\t\tled
\t\t\t\t\t\t\t\t\tthe school through some major challenges in education; with the former supervising the change from a five
\t\t\t\t\t\t\t\t\tyear
\t\t\t\t\t\t\t\t\tcourse of study to a six year program: three years (Grades 7 to 9) of junior secondary and three years
\t\t\t\t\t\t\t\t\t(Grades
\t\t\t\t\t\t\t\t\t10 -12) of senior secondary, during which time St. Joseph’s had to go double shift. The school went
\t\t\t\t\t\t\t\t\tthrough yet
\t\t\t\t\t\t\t\t\tanother change spearheaded by Sr. Catherine, this saw the transition to a single shift school offering
\t\t\t\t\t\t\t\t\tthree
\t\t\t\t\t\t\t\t\tyears of senior secondary education. Through all these challenges, these brave women never lost sight of
\t\t\t\t\t\t\t\t\tthe
\t\t\t\t\t\t\t\t\tinfrastructural development of the school, thus the building of the school hall by Mrs. Ndure and the
\t\t\t\t\t\t\t\t\tcomputer
\t\t\t\t\t\t\t\t\tlaboratory by Sr. Catherine.
\t\t\t\t\t\t\t\t\tDuring this time, THE CURRICULUM broadened greatly, as the school strived to meet the educational
\t\t\t\t\t\t\t\t\trequirements
\t\t\t\t\t\t\t\t\tof parents and students in a modern world. To include 24 subjects at the West African Senior School
\t\t\t\t\t\t\t\t\tCertificate
\t\t\t\t\t\t\t\t\tExamination (WASSCE), which replaced the General Certificate in Education (GCE) exams. We also offer
\t\t\t\t\t\t\t\t\tnon-examinable subjects like Information Technology and Physical Education. Our undertaking as an
\t\t\t\t\t\t\t\t\teducational
\t\t\t\t\t\t\t\t\tcommunity is the pursuit of excellence and the development of the full potential for all.
\t\t\t\t\t\t\t\t\tIn this 21st Century, St. Joseph’s recognizes the importance of IT as an essential tool to facilitate
\t\t\t\t\t\t\t\t\tbetter,
\t\t\t\t\t\t\t\t\teffective and efficient learning in school.
\t\t\t\t\t\t\t\t\tWe endeavour, through the use of ICT to make quality education accessible to all students.
\t\t\t\t\t\t\t\t\tOur Research Laboratory (a converted classroom) was unveiled in December 2011. Here students and teachers
\t\t\t\t\t\t\t\t\tcan
\t\t\t\t\t\t\t\t\taccess the internet for research purposes. With the installation of the “Smart Room” in 2012, equipped
\t\t\t\t\t\t\t\t\twith an
\t\t\t\t\t\t\t\t\telectronic board, a projector, lap-top and responders, both teachers and students are exposed to IT
\t\t\t\t\t\t\t\t\tmethods of
\t\t\t\t\t\t\t\t\tteaching and learning especially in Maths and Physics. The Progressive Maths and Science Initiatives
\t\t\t\t\t\t\t\t\tfunded by
\t\t\t\t\t\t\t\t\tthe Ministry of Basic and Secondary Education (MOBSE) have contributed immensely in improving
\t\t\t\t\t\t\t\t\tperformance,
\t\t\t\t\t\t\t\t\tespecially in Physics, at WASSCE. To facilitate this expansion and also help cut on the associated energy
\t\t\t\t\t\t\t\t\tcosts
\t\t\t\t\t\t\t\t\tfor running such a programme, the school has is now equipped with a Solar System to power the Science
\t\t\t\t\t\t\t\t\tBlock
\t\t\t\t\t\t\t\t\tsince August 2017.

\t\t\t\t\t\t\t\t\tThe nature of students that come into St. Joseph’s today, warrants that we work relentlessly to bring out
\t\t\t\t\t\t\t\t\tand
\t\t\t\t\t\t\t\t\tnurture their God given talents, through taught lessons and extracurricular activities; continuously
\t\t\t\t\t\t\t\t\tembarking
\t\t\t\t\t\t\t\t\ton new initiatives and trying to consolidate on pre-existing ones both within and outside the classroom
\t\t\t\t\t\t\t\t\tas we
\t\t\t\t\t\t\t\t\tseek to provide a holistic approach to the education of our girls. St. Joseph’s believes strongly in
\t\t\t\t\t\t\t\t\tcreating
\t\t\t\t\t\t\t\t\topportunities for the girl child. We have introduced a Vocational Class in September 2015, registering
\t\t\t\t\t\t\t\t\tstudents
\t\t\t\t\t\t\t\t\twith aggregates 37-42. This class serves as a pre-ten class so that students who do well in this class
\t\t\t\t\t\t\t\t\twill be
\t\t\t\t\t\t\t\t\tgiven the opportunity in mainstream in the subsequent academic year. The rationale behind the vocational
\t\t\t\t\t\t\t\t\tclass
\t\t\t\t\t\t\t\t\tis to give students who may be late developers academically as well as those from the lower tier of
\t\t\t\t\t\t\t\t\tsociety a
\t\t\t\t\t\t\t\t\tchance to access secondary education.

\t\t\t\t\t\t\t\t\tSport continues to play a huge role in the development of our young Ladies. St. Joseph’s continues to be
\t\t\t\t\t\t\t\t\trated
\t\t\t\t\t\t\t\t\thighly competitive in a number of sporting codes. The basketball, volleyball, football and cricket teams
\t\t\t\t\t\t\t\t\thave
\t\t\t\t\t\t\t\t\tall participated in competitions and managed to reach respectable stages of their various tournaments, as
\t\t\t\t\t\t\t\t\tdefending champions for Volleyball and current champions for Cricket. However, the past three years have
\t\t\t\t\t\t\t\t\tbeen
\t\t\t\t\t\t\t\t\tparticularly bad for athletics.

\t\t\t\t\t\t\t\t\tFrom 2011 to 2015, we have seen a positive trend in performance at WASSCE. There has been a steady
\t\t\t\t\t\t\t\t\tincrease in
\t\t\t\t\t\t\t\t\tthe number of candidates scoring grades A, B and C (credit passes) in various subjects. In 2014, St.
\t\t\t\t\t\t\t\t\tJoseph’s
\t\t\t\t\t\t\t\t\tranked 7th out of 74 senior secondary schools in the Gambia at WASSCE. Though this may look grim to us
\t\t\t\t\t\t\t\t\tJosephina’s and Augustinian’s, the school prides itself in the fact that we actually add value to
\t\t\t\t\t\t\t\t\tacademic
\t\t\t\t\t\t\t\t\tstanding of our students: ‘we do not take the best, we create the best’.
\t\t\t\t\t\t\t\t\tToday, I am very confident as Principal, to say YES INDEED St. Joseph’s is making a comeback! In the year
\t\t\t\t\t\t\t\t\t2016,
\t\t\t\t\t\t\t\t\tSt. Joseph’s set major record at WASSCE: for the first time in the history of this noble institution we
\t\t\t\t\t\t\t\t\tproduced
\t\t\t\t\t\t\t\t\tsix candidates with 9 straight credits: three from the science class and three from the commerce class.

\t\t\t\t\t\t\t\t\tOur ultimate goal as an educational institution is: “to make St. Joseph’s a Model School in the Gambia
\t\t\t\t\t\t\t\t\tthat can
\t\t\t\t\t\t\t\t\tcompete favourably in this millennium, through the adoption of best practices”

\t\t\t\t\t\t\t\t\tIn strategic terms, St. Joseph’s Senior Secondary School now needs to consolidate its identity and to
\t\t\t\t\t\t\t\t\tstrengthen
\t\t\t\t\t\t\t\t\tits educational quality. We need to rebuild St. Joseph’s and bring it back to the RADAR of Academic and
\t\t\t\t\t\t\t\t\tSporting
\t\t\t\t\t\t\t\t\tExcellence. For this we need the support of philanthropists, well wishers and most especially our
\t\t\t\t\t\t\t\t\talumnae.
\t\t\t\t\t\t\t\t\tI have the greatest pleasure both as principal and someone who has benefitted so much from this noble
\t\t\t\t\t\t\t\t\tinstitution to be associated with the maiden Reunion of the Saints Alumni Association Europe Chapter.
\t\t\t\t\t\t\t\t\tPlease
\t\t\t\t\t\t\t\t\tremember that whatever spending you make as part of this activity is not an expense but an investment in
\t\t\t\t\t\t\t\t\tthe
\t\t\t\t\t\t\t\t\tdevelopment of Your Alma Mata! <br>

\t\t\t\t\t\t\t\t\tTogether we can bring back the glory days of the Mighty Saints! The future is bright indeed! <br>

\t\t\t\t\t\t\t\t\tORA ET LABORA!!
\t\t\t\t\t\t\t\t\t(Pray and Work!) <br>

\t\t\t\t\t\t\t\t\tLong live St. Josephs! <br>


\t\t\t\t\t\t\t\t\tSigned: Ms. Hannah K Coker <br>
\t\t\t\t\t\t\t\t\tPrincipal
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t\t<div id=\"fh5co-explore\" class=\"fh5co-bg-section\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t\t<h2>Code of Conduct</h2>
\t\t\t\t\t\t\t<p> The realization of these are what makes us proud Josephina's.</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 animate-box\">
\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<h4><i class=\"icon-eye\"></i>Vision</h4>
\t\t\t\t\t\t\t\t<p>&ldquo;To make St. Joseph's a Model School in The Gambia through the gates of St. Joseph's will, at the end
\t\t\t\t\t\t\t\t\tof their three years education, be employable citizens of The Gambia, either employed or self-employed, and
\t\t\t\t\t\t\t\t\twho would be financially independent to be able to take care of themselves, their families, and the community
\t\t\t\t\t\t\t\t\tat large.&ldquo;</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- <div>
\t\t\t\t\t\t\t\t<h4><i class=\"icon-video2\"></i>Objectives</h4>
\t\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there
\t\t\t\t\t\t\t\t\tlive
\t\t\t\t\t\t\t\t\tthe blind texts. </p>
\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-6 animate-box\">
\t\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<h4><i class=\"icon-light-bulb\"></i>Mission Statement</h4>
\t\t\t\t\t\t\t\t\t\t<p>&ldquo;To send out at the end of their schooling, educated, loyal, 
\t\t\t\t\t\t\t\t\t\t\tmorally adn religiously strong and talented young women who would serve their nation with pride.&ldquo;
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t\t<!-- <div class=\"row animate-box\">
\t\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t\t\t\t<h4><i class=\"icon-video2\"></i>The St. Joseph’s Experience is very powerful</h4>
\t\t\t\t\t\t\t\t\t<p style=\"text-align: justify;\">St. Joseph’s, through its Vision and Mission, stands to develop rounded individuals and continuously
\t\t\t\t\t\t\t\t\t\tseeks to set the highest standards.</p>
\t\t\t\t\t\t\t\t<p style=\"text-align: justify;\">
\t\t\t\t\t\t\t\t\tIt gives students a unique sense of boldness. <br>
\t\t\t\t\t\t\t\t\tIt helps them to develop their God given talents and potentials. <br>
\t\t\t\t\t\t\t\t\tIt gives the girls the opportunity to explore different disciplines and integrate fields of knowledge,
\t\t\t\t\t\t\t\t\tand, to ask the right questions here students are uniquely prepared to not only fit in but to excel wherever
\t\t\t\t\t\t\t\t\tthey may find themselves.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> -->

\t\t\t\t\t\t<div class=\"col-md-6 animate-box text-center\">
\t\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t</div>


\t<div id=\"fh5co-testimonial\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2 style=\"margin-bottom: -2em;\">Head Of Departments</h2>
\t\t\t\t\t\t<p class=\"mt-100\">We have had a numberous challenges but our Heads in various departments are always on-guard.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-10 col-md-offset-1\">
\t\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t\t<div class=\"owl-carousel owl-carousel-fullwidth\">
\t\t\t\t\t\t\t";
        // line 344
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 344);
        // line 345
        echo "\t\t\t\t\t\t\t";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 345);
        // line 346
        echo "\t\t\t\t\t\t\t";
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 346);
        // line 347
        echo "\t\t\t\t\t\t\t";
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 347);
        // line 348
        echo "\t\t\t\t\t\t\t";
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 348);
        // line 349
        echo "\t\t\t\t\t\t\t";
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 349);
        // line 350
        echo "
\t\t\t\t\t\t\t";
        // line 351
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 352
            echo "\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t<div class=\"testimony-slide active text-center\">
\t\t\t\t\t\t\t\t\t\t<figure>
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
            // line 355
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "headofdept_img", [], "any", false, false, false, 355), "thumb", [0 => 100, 1 => 100, 2 => ["mode" => "crop"]], "method", false, false, false, 355), "html", null, true);
            echo "\" alt=\"user\">
\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t<span>";
            // line 357
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 357), "html", null, true);
            echo " <br> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "position", [], "any", false, false, false, 357), "html", null, true);
            echo "</span>
\t\t\t\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t\t\t\t<p>&ldquo;";
            // line 359
            echo twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 359);
            echo "&rdquo;</p>
\t\t\t\t\t\t\t\t\t\t</blockquote>

\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<!-- <p>Our Teachers</p>

\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"testimony-slide active text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
            // line 372
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "headofdept_img", [], "any", false, false, false, 372), "thumb", [0 => 100, 1 => 100, 2 => ["mode" => "crop"]], "method", false, false, false, 372), "html", null, true);
            echo "\" alt=\"user\">
\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>&ldquo;";
            // line 376
            echo twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 376);
            echo "&rdquo;</p>
\t\t\t\t\t\t\t\t\t\t\t\t</blockquote>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div> -->

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 385
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/about.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  464 => 385,  449 => 376,  442 => 372,  426 => 359,  419 => 357,  414 => 355,  409 => 352,  405 => 351,  402 => 350,  399 => 349,  396 => 348,  393 => 347,  390 => 346,  387 => 345,  385 => 344,  112 => 74,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\" style=\"background-image:url({{ 'assets/images/picture.jpg'|theme }});\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Know About SJSSS</h1>
\t\t\t\t\t\t\t<h2>We are an all-girl school. Thus we're proud of who we are as Josephina's.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>

\t<div id=\"fh5co-counter\" class=\"fh5co-counters\" style=\"background-color: #f2f2f2;\">
\t\t<div class=\"container\">
\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t<h2>Our School By Numbers</h2>
\t\t\t\t\t\t<p>The statistics are what make us keep going, thus constant improvement would be achieved.</P>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span id=\"icons\" class=\"icon-book\"></span>
\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">1:20</h2></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Students to Teachers Ratio</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span id=\"icons\" class=\"icon-trophy\"></span>
\t\t\t\t\t\t<spa>
\t\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">100%</h2></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Participation In Extra Currillum Activities</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span id=\"icons\" class=\"icon-graduation-cap\"></span>
\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">15</h2></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Graduate Teachers</span>
\t\t\t\t</div>\t
\t\t\t</div>

\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-6 text-center animate-box\">
\t\t\t\t\t\t\t<span id=\"icons\" class=\"icon-graduation-cap\"></span>
\t\t\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">95%</h2></span>
\t\t\t\t\t\t\t<span class=\"fh5co-counter-label\">Students Graduation Rate</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-6 text-center animate-box\">
\t\t\t\t\t\t\t\t<span id=\"icons\" class=\"icon-hand\"></span>
\t\t\t\t\t\t\t\t<h2 n class=\"mt-20\" style=\"font-size: 44px; color: rgba(0, 0, 0, 0.5);\">15</h2></span>
\t\t\t\t\t\t\t\t<span class=\"fh5co-counter-label\">Enrolled In Vocational Stream</span>
\t\t\t\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"fh5co-explore\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2>About Us</h2>
\t\t\t\t\t<p>
\t\t\t\t\t    We have gone through so much for almost a century, browse through our history, know us more.
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"fh5co-explore fh5co-explore1\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-5 col-md-push-7 animate-box mt-60\">
\t\t\t\t\t\t<img class=\"img-responsive abt-img\" src=\"{{ 'assets/images/principal.png'|theme }}\" alt=\"work\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-7 col-md-pull-5 animate-box\">
\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t<h3>We Are The Great St. Josephs</h3>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tSince 1883, the Sisters of St. Joseph of Cluny have worked in The Gambia with devotion and dedication in the
\t\t\t\t\t\t\t\t\tfields of Education, Nursing and other social work for the promotion of the full development of the Gambian
\t\t\t\t\t\t\t\t\tpeople.
\t\t\t\t\t\t\t\t\tSt. Joseph’s Senior Secondary School (previously St. Joseph’s High School) was established in 1921 as a Roman
\t\t\t\t\t\t\t\t\tCatholic Mission School for Girls by the Congregation with an initial enrolment of forty-four students headed
\t\t\t\t\t\t\t\t\tby Mrs. Nellie Morris who handed over to Sr. Celeste Wall in 1923. The school’s policy was to instil self
\t\t\t\t\t\t\t\t\tdiscipline, inculcate moral values and to provide a model education for its students, Christians and Muslims
\t\t\t\t\t\t\t\t\talike; with basic reading, writing, arithmetic, music (introduced in 1926), home economics skills with a great
\t\t\t\t\t\t\t\t\temphasis on religious education. The Cluny Sisters at the time believed that education provided skills for
\t\t\t\t\t\t\t\t\tgirls to become good home managers and disciplined citizens within their communities.
\t\t\t\t\t\t\t\t\tOn the 10th of April 1940, Sr. Jarlath Hoade took over the mantle, the only principal to head both the
\t\t\t\t\t\t\t\t\tsecondary and primary schools concurrently.
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t
\t\t\t\t\t<!-- Button trigger modal -->
\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalScrollable\">
\t\t\t\t\t\tContinue Reading 
\t\t\t\t\t</button>

\t\t\t\t\t<!-- Modal -->
\t\t\t\t\t<div class=\"modal fade\" id=\"exampleModalScrollable\" tabindex=\"-1\" role=\"dialog\"
\t\t\t\t\t\taria-labelledby=\"exampleModalScrollableTitle\" aria-hidden=\"true\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">
\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalScrollableTitle\">All About Our History</h5>
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\tThe period running up to and soon after independence in 1965, brought numerous reforms in the education
\t\t\t\t\t\t\t\t\tsystem
\t\t\t\t\t\t\t\t\tin the Gambia. Student Enrolment increased over the years as the demand for secondary education escalated
\t\t\t\t\t\t\t\t\tin the
\t\t\t\t\t\t\t\t\tcountry; St. Joseph’s grew to a two stream school in 1975, necessitating the move to its present location
\t\t\t\t\t\t\t\t\tat
\t\t\t\t\t\t\t\t\tCampama in 1976 through the leadership of Sr. Benigna Kearnet, who later handed over to Sr. Theresa
\t\t\t\t\t\t\t\t\tMundow. She
\t\t\t\t\t\t\t\t\twas succeeded by Mrs. Elsie Shyngle who passed the baton on to Sr. Leo. All these remarkable women,
\t\t\t\t\t\t\t\t\tcontributed
\t\t\t\t\t\t\t\t\tgreatly to the development of St. Joseph’s.

\t\t\t\t\t\t\t\t\tThe importance of education in the life of an individual cannot be overstated. Education shapes
\t\t\t\t\t\t\t\t\tindividuals in
\t\t\t\t\t\t\t\t\ttheir formative years; and imbues and inculcates them with morals and values that would govern all their
\t\t\t\t\t\t\t\t\tchoices
\t\t\t\t\t\t\t\t\tin life. This is the raison d’être of St. Joseph’s as embodied in our MISSION.
\t\t\t\t\t\t\t\t\tWith the ever increasing changes in the world and the rapid trends in development, it became even more
\t\t\t\t\t\t\t\t\timportant
\t\t\t\t\t\t\t\t\tto equip our girls with the prerequisite skills and knowledge that will help them fit in a global
\t\t\t\t\t\t\t\t\tvillage. The
\t\t\t\t\t\t\t\t\tAnne Marie Javouhey Libray built in the 80’s remains an important facility for our girls. At St. Joseph’s
\t\t\t\t\t\t\t\t\twe
\t\t\t\t\t\t\t\t\tstand always and forever committed to providing our students the best of opportunities so that each GIRL
\t\t\t\t\t\t\t\t\tgets a
\t\t\t\t\t\t\t\t\thead start in life, to reach the dizzy heights scaled by many of our distinguished alumnae.

\t\t\t\t\t\t\t\t\tI took over task of carrying this ethos forward on the 5th January 2011 from Sr. Catherine Jarra who took
\t\t\t\t\t\t\t\t\tover
\t\t\t\t\t\t\t\t\tin 2000 and who was preceded by Mrs. Vicky Ndure, principal from 1993. These two strong and dynamic women
\t\t\t\t\t\t\t\t\tled
\t\t\t\t\t\t\t\t\tthe school through some major challenges in education; with the former supervising the change from a five
\t\t\t\t\t\t\t\t\tyear
\t\t\t\t\t\t\t\t\tcourse of study to a six year program: three years (Grades 7 to 9) of junior secondary and three years
\t\t\t\t\t\t\t\t\t(Grades
\t\t\t\t\t\t\t\t\t10 -12) of senior secondary, during which time St. Joseph’s had to go double shift. The school went
\t\t\t\t\t\t\t\t\tthrough yet
\t\t\t\t\t\t\t\t\tanother change spearheaded by Sr. Catherine, this saw the transition to a single shift school offering
\t\t\t\t\t\t\t\t\tthree
\t\t\t\t\t\t\t\t\tyears of senior secondary education. Through all these challenges, these brave women never lost sight of
\t\t\t\t\t\t\t\t\tthe
\t\t\t\t\t\t\t\t\tinfrastructural development of the school, thus the building of the school hall by Mrs. Ndure and the
\t\t\t\t\t\t\t\t\tcomputer
\t\t\t\t\t\t\t\t\tlaboratory by Sr. Catherine.
\t\t\t\t\t\t\t\t\tDuring this time, THE CURRICULUM broadened greatly, as the school strived to meet the educational
\t\t\t\t\t\t\t\t\trequirements
\t\t\t\t\t\t\t\t\tof parents and students in a modern world. To include 24 subjects at the West African Senior School
\t\t\t\t\t\t\t\t\tCertificate
\t\t\t\t\t\t\t\t\tExamination (WASSCE), which replaced the General Certificate in Education (GCE) exams. We also offer
\t\t\t\t\t\t\t\t\tnon-examinable subjects like Information Technology and Physical Education. Our undertaking as an
\t\t\t\t\t\t\t\t\teducational
\t\t\t\t\t\t\t\t\tcommunity is the pursuit of excellence and the development of the full potential for all.
\t\t\t\t\t\t\t\t\tIn this 21st Century, St. Joseph’s recognizes the importance of IT as an essential tool to facilitate
\t\t\t\t\t\t\t\t\tbetter,
\t\t\t\t\t\t\t\t\teffective and efficient learning in school.
\t\t\t\t\t\t\t\t\tWe endeavour, through the use of ICT to make quality education accessible to all students.
\t\t\t\t\t\t\t\t\tOur Research Laboratory (a converted classroom) was unveiled in December 2011. Here students and teachers
\t\t\t\t\t\t\t\t\tcan
\t\t\t\t\t\t\t\t\taccess the internet for research purposes. With the installation of the “Smart Room” in 2012, equipped
\t\t\t\t\t\t\t\t\twith an
\t\t\t\t\t\t\t\t\telectronic board, a projector, lap-top and responders, both teachers and students are exposed to IT
\t\t\t\t\t\t\t\t\tmethods of
\t\t\t\t\t\t\t\t\tteaching and learning especially in Maths and Physics. The Progressive Maths and Science Initiatives
\t\t\t\t\t\t\t\t\tfunded by
\t\t\t\t\t\t\t\t\tthe Ministry of Basic and Secondary Education (MOBSE) have contributed immensely in improving
\t\t\t\t\t\t\t\t\tperformance,
\t\t\t\t\t\t\t\t\tespecially in Physics, at WASSCE. To facilitate this expansion and also help cut on the associated energy
\t\t\t\t\t\t\t\t\tcosts
\t\t\t\t\t\t\t\t\tfor running such a programme, the school has is now equipped with a Solar System to power the Science
\t\t\t\t\t\t\t\t\tBlock
\t\t\t\t\t\t\t\t\tsince August 2017.

\t\t\t\t\t\t\t\t\tThe nature of students that come into St. Joseph’s today, warrants that we work relentlessly to bring out
\t\t\t\t\t\t\t\t\tand
\t\t\t\t\t\t\t\t\tnurture their God given talents, through taught lessons and extracurricular activities; continuously
\t\t\t\t\t\t\t\t\tembarking
\t\t\t\t\t\t\t\t\ton new initiatives and trying to consolidate on pre-existing ones both within and outside the classroom
\t\t\t\t\t\t\t\t\tas we
\t\t\t\t\t\t\t\t\tseek to provide a holistic approach to the education of our girls. St. Joseph’s believes strongly in
\t\t\t\t\t\t\t\t\tcreating
\t\t\t\t\t\t\t\t\topportunities for the girl child. We have introduced a Vocational Class in September 2015, registering
\t\t\t\t\t\t\t\t\tstudents
\t\t\t\t\t\t\t\t\twith aggregates 37-42. This class serves as a pre-ten class so that students who do well in this class
\t\t\t\t\t\t\t\t\twill be
\t\t\t\t\t\t\t\t\tgiven the opportunity in mainstream in the subsequent academic year. The rationale behind the vocational
\t\t\t\t\t\t\t\t\tclass
\t\t\t\t\t\t\t\t\tis to give students who may be late developers academically as well as those from the lower tier of
\t\t\t\t\t\t\t\t\tsociety a
\t\t\t\t\t\t\t\t\tchance to access secondary education.

\t\t\t\t\t\t\t\t\tSport continues to play a huge role in the development of our young Ladies. St. Joseph’s continues to be
\t\t\t\t\t\t\t\t\trated
\t\t\t\t\t\t\t\t\thighly competitive in a number of sporting codes. The basketball, volleyball, football and cricket teams
\t\t\t\t\t\t\t\t\thave
\t\t\t\t\t\t\t\t\tall participated in competitions and managed to reach respectable stages of their various tournaments, as
\t\t\t\t\t\t\t\t\tdefending champions for Volleyball and current champions for Cricket. However, the past three years have
\t\t\t\t\t\t\t\t\tbeen
\t\t\t\t\t\t\t\t\tparticularly bad for athletics.

\t\t\t\t\t\t\t\t\tFrom 2011 to 2015, we have seen a positive trend in performance at WASSCE. There has been a steady
\t\t\t\t\t\t\t\t\tincrease in
\t\t\t\t\t\t\t\t\tthe number of candidates scoring grades A, B and C (credit passes) in various subjects. In 2014, St.
\t\t\t\t\t\t\t\t\tJoseph’s
\t\t\t\t\t\t\t\t\tranked 7th out of 74 senior secondary schools in the Gambia at WASSCE. Though this may look grim to us
\t\t\t\t\t\t\t\t\tJosephina’s and Augustinian’s, the school prides itself in the fact that we actually add value to
\t\t\t\t\t\t\t\t\tacademic
\t\t\t\t\t\t\t\t\tstanding of our students: ‘we do not take the best, we create the best’.
\t\t\t\t\t\t\t\t\tToday, I am very confident as Principal, to say YES INDEED St. Joseph’s is making a comeback! In the year
\t\t\t\t\t\t\t\t\t2016,
\t\t\t\t\t\t\t\t\tSt. Joseph’s set major record at WASSCE: for the first time in the history of this noble institution we
\t\t\t\t\t\t\t\t\tproduced
\t\t\t\t\t\t\t\t\tsix candidates with 9 straight credits: three from the science class and three from the commerce class.

\t\t\t\t\t\t\t\t\tOur ultimate goal as an educational institution is: “to make St. Joseph’s a Model School in the Gambia
\t\t\t\t\t\t\t\t\tthat can
\t\t\t\t\t\t\t\t\tcompete favourably in this millennium, through the adoption of best practices”

\t\t\t\t\t\t\t\t\tIn strategic terms, St. Joseph’s Senior Secondary School now needs to consolidate its identity and to
\t\t\t\t\t\t\t\t\tstrengthen
\t\t\t\t\t\t\t\t\tits educational quality. We need to rebuild St. Joseph’s and bring it back to the RADAR of Academic and
\t\t\t\t\t\t\t\t\tSporting
\t\t\t\t\t\t\t\t\tExcellence. For this we need the support of philanthropists, well wishers and most especially our
\t\t\t\t\t\t\t\t\talumnae.
\t\t\t\t\t\t\t\t\tI have the greatest pleasure both as principal and someone who has benefitted so much from this noble
\t\t\t\t\t\t\t\t\tinstitution to be associated with the maiden Reunion of the Saints Alumni Association Europe Chapter.
\t\t\t\t\t\t\t\t\tPlease
\t\t\t\t\t\t\t\t\tremember that whatever spending you make as part of this activity is not an expense but an investment in
\t\t\t\t\t\t\t\t\tthe
\t\t\t\t\t\t\t\t\tdevelopment of Your Alma Mata! <br>

\t\t\t\t\t\t\t\t\tTogether we can bring back the glory days of the Mighty Saints! The future is bright indeed! <br>

\t\t\t\t\t\t\t\t\tORA ET LABORA!!
\t\t\t\t\t\t\t\t\t(Pray and Work!) <br>

\t\t\t\t\t\t\t\t\tLong live St. Josephs! <br>


\t\t\t\t\t\t\t\t\tSigned: Ms. Hannah K Coker <br>
\t\t\t\t\t\t\t\t\tPrincipal
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t\t<div id=\"fh5co-explore\" class=\"fh5co-bg-section\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t\t<h2>Code of Conduct</h2>
\t\t\t\t\t\t\t<p> The realization of these are what makes us proud Josephina's.</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 animate-box\">
\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t<h4><i class=\"icon-eye\"></i>Vision</h4>
\t\t\t\t\t\t\t\t<p>&ldquo;To make St. Joseph's a Model School in The Gambia through the gates of St. Joseph's will, at the end
\t\t\t\t\t\t\t\t\tof their three years education, be employable citizens of The Gambia, either employed or self-employed, and
\t\t\t\t\t\t\t\t\twho would be financially independent to be able to take care of themselves, their families, and the community
\t\t\t\t\t\t\t\t\tat large.&ldquo;</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- <div>
\t\t\t\t\t\t\t\t<h4><i class=\"icon-video2\"></i>Objectives</h4>
\t\t\t\t\t\t\t\t<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there
\t\t\t\t\t\t\t\t\tlive
\t\t\t\t\t\t\t\t\tthe blind texts. </p>
\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-6 animate-box\">
\t\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<h4><i class=\"icon-light-bulb\"></i>Mission Statement</h4>
\t\t\t\t\t\t\t\t\t\t<p>&ldquo;To send out at the end of their schooling, educated, loyal, 
\t\t\t\t\t\t\t\t\t\t\tmorally adn religiously strong and talented young women who would serve their nation with pride.&ldquo;
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t\t<!-- <div class=\"row animate-box\">
\t\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t\t\t\t<h4><i class=\"icon-video2\"></i>The St. Joseph’s Experience is very powerful</h4>
\t\t\t\t\t\t\t\t\t<p style=\"text-align: justify;\">St. Joseph’s, through its Vision and Mission, stands to develop rounded individuals and continuously
\t\t\t\t\t\t\t\t\t\tseeks to set the highest standards.</p>
\t\t\t\t\t\t\t\t<p style=\"text-align: justify;\">
\t\t\t\t\t\t\t\t\tIt gives students a unique sense of boldness. <br>
\t\t\t\t\t\t\t\t\tIt helps them to develop their God given talents and potentials. <br>
\t\t\t\t\t\t\t\t\tIt gives the girls the opportunity to explore different disciplines and integrate fields of knowledge,
\t\t\t\t\t\t\t\t\tand, to ask the right questions here students are uniquely prepared to not only fit in but to excel wherever
\t\t\t\t\t\t\t\t\tthey may find themselves.
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> -->

\t\t\t\t\t\t<div class=\"col-md-6 animate-box text-center\">
\t\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t</div>


\t<div id=\"fh5co-testimonial\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2 style=\"margin-bottom: -2em;\">Head Of Departments</h2>
\t\t\t\t\t\t<p class=\"mt-100\">We have had a numberous challenges but our Heads in various departments are always on-guard.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-10 col-md-offset-1\">
\t\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t\t<div class=\"owl-carousel owl-carousel-fullwidth\">
\t\t\t\t\t\t\t{% set records = builderList.records %}
\t\t\t\t\t\t\t{% set displayColumn = builderList.displayColumn %}
\t\t\t\t\t\t\t{% set noRecordsMessage = builderList.noRecordsMessage %}
\t\t\t\t\t\t\t{% set detailsPage = builderList.detailsPage %}
\t\t\t\t\t\t\t{% set detailsKeyColumn = builderList.detailsKeyColumn %}
\t\t\t\t\t\t\t{% set detailsUrlParameter = builderList.detailsUrlParameter %}

\t\t\t\t\t\t\t{% for record in records %}
\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t<div class=\"testimony-slide active text-center\">
\t\t\t\t\t\t\t\t\t\t<figure>
\t\t\t\t\t\t\t\t\t\t\t<img src=\"{{ record.headofdept_img.thumb(100,100, {'mode': 'crop'}) }}\" alt=\"user\">
\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t<span>{{ record.name }} <br> {{ record.position }}</span>
\t\t\t\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t\t\t\t<p>&ldquo;{{ record.description|raw }}&rdquo;</p>
\t\t\t\t\t\t\t\t\t\t</blockquote>

\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<!-- <p>Our Teachers</p>

\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"testimony-slide active text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"{{ record.headofdept_img.thumb(100,100, {'mode': 'crop'}) }}\" alt=\"user\">
\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<blockquote>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>&ldquo;{{ record.description|raw }}&rdquo;</p>
\t\t\t\t\t\t\t\t\t\t\t\t</blockquote>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div> -->

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t
</div>
</div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/about.htm", "");
    }
}
