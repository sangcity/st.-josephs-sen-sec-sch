<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/gallery.htm */
class __TwigTemplate_8331049b1bbef7535778490cc845537bbb302cd94955f38371a4b024ed785c81 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\" style=\"background-image:url(";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/picture.jpg");
        echo ");\" data-stellar-background-ratio=\"0.5\">
            <div class=\"overlay\"></div>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8 col-md-offset-2 text-center\">
                        <div class=\"display-t\">
                            <div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
                                <h1>Gallery</h1>
                                <h2>We are an all-girl school. Thus we're proud of our achievements in the diverse world of academics.</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</header>

\t<div id=\"fh5co-blog\">
\t\t<div class=\"row animate-box\">
\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t<h2>Great Times</h2>
\t\t\t\t<p>See the great fun and experience our students have always made us exprience in this unbelieveable environment of Art, Academics, Sports, Entertainment, etc.</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container-fluid proj-bottom\">
\t\t\t<div class=\"row\">
\t\t\t";
        // line 26
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 26);
        // line 27
        echo "\t\t\t";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 27);
        // line 28
        echo "\t\t\t";
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 28);
        // line 29
        echo "\t\t\t";
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 29);
        // line 30
        echo "\t\t\t";
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 30);
        // line 31
        echo "\t\t\t";
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 31);
        // line 32
        echo "
\t\t\t\t";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 34
            echo "
\t\t\t\t\t";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["record"], "gallery", [], "any", false, false, false, 35));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 36
                echo "
\t\t\t\t\t";
                // line 37
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 37) < 13)) {
                    // line 38
                    echo "
\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6 fh5co-project animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t<img src=\"";
                    // line 41
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [0 => 392, 1 => 261, 2 => ["mode" => "crop"]], "method", false, false, false, 41), "html", null, true);
                    echo "\" alt=\"Free HTML5 Website Template by FreeHTML5.co\" class=\"img-responsive\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t\t";
                }
                // line 46
                echo "
\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "\t\t\t\t
\t\t\t</div>
\t\t
\t\t</div>
\t</div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/gallery.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 50,  141 => 48,  126 => 46,  118 => 41,  113 => 38,  111 => 37,  108 => 36,  91 => 35,  88 => 34,  84 => 33,  81 => 32,  78 => 31,  75 => 30,  72 => 29,  69 => 28,  66 => 27,  64 => 26,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\" style=\"background-image:url({{ 'assets/images/picture.jpg'|theme }});\" data-stellar-background-ratio=\"0.5\">
            <div class=\"overlay\"></div>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8 col-md-offset-2 text-center\">
                        <div class=\"display-t\">
                            <div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
                                <h1>Gallery</h1>
                                <h2>We are an all-girl school. Thus we're proud of our achievements in the diverse world of academics.</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</header>

\t<div id=\"fh5co-blog\">
\t\t<div class=\"row animate-box\">
\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t<h2>Great Times</h2>
\t\t\t\t<p>See the great fun and experience our students have always made us exprience in this unbelieveable environment of Art, Academics, Sports, Entertainment, etc.</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container-fluid proj-bottom\">
\t\t\t<div class=\"row\">
\t\t\t{% set records = builderList.records %}
\t\t\t{% set displayColumn = builderList.displayColumn %}
\t\t\t{% set noRecordsMessage = builderList.noRecordsMessage %}
\t\t\t{% set detailsPage = builderList.detailsPage %}
\t\t\t{% set detailsKeyColumn = builderList.detailsKeyColumn %}
\t\t\t{% set detailsUrlParameter = builderList.detailsUrlParameter %}

\t\t\t\t{% for record in records %}

\t\t\t\t\t{% for image in record.gallery %}

\t\t\t\t\t{% if loop.index < 13 %}

\t\t\t\t\t\t<div class=\"col-md-3 col-sm-6 fh5co-project animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t<img src=\"{{ image.thumb(392,261,{'mode':'crop'}) }}\" alt=\"Free HTML5 Website Template by FreeHTML5.co\" class=\"img-responsive\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t\t{% endif %}

\t\t\t\t\t{% endfor %}

\t\t\t\t{% endfor %}
\t\t\t\t
\t\t\t</div>
\t\t
\t\t</div>
\t</div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/gallery.htm", "");
    }
}
