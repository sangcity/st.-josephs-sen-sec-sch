<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/partials/footer.htm */
class __TwigTemplate_e8954b5c849c4be2aaf528e337ab6bed631887e0dd5052b1a273f94547c305b7 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<footer id=\"fh5co-footer\" role=\"contentinfo\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row row-pb-md\">
\t\t\t\t<div class=\"col-md-4 fh5co-widget\">
\t\t\t\t\t<h4>St. Joseph's Sen. Sec. Sch.</h4>
\t\t\t\t\t<ul class=\"fh5co-footer-links\">
\t\t\t\t\t\t<li>P.O Box 119</li>
\t\t\t\t\t\t<li>Box Bar Road.</li>
\t\t\t\t\t\t<li>+220 ‭422-7709</li>
\t\t\t\t\t\t<li>stjosephs1921@yahoo.co.uk</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 col-sm-4 col-xs-6 col-md-push-1\">
\t\t\t\t\t<h4>Work &amp; Pray</h4>
\t\t\t\t\t<ul class=\"fh5co-footer-links\">
\t\t\t\t\t\t<li><a href=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">Home</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 17
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about");
        echo "\">About</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("events");
        echo "\">Events</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\">Blog</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("gallery");
        echo "\">Gallery</a></li>
\t\t\t\t\t\t<li><a href=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">Contact</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-4 col-sm-4 col-xs-6 col-md-push-1\">
\t\t\t\t\t<h4>School Hours</h4>
\t\t\t\t\t<ul class=\"fh5co-footer-links\">
\t\t\t\t\t\t<li>8:15 a.m. Classes Begin</li>
\t\t\t\t\t\t<li>10:55 a.m. Break Period</li>
\t\t\t\t\t\t<li>11:30 a.m. Classes Begin</li>
\t\t\t\t\t\t<li>13:55 p.m. Closing Period</li>
\t\t\t\t\t\t<li>14:15 p.m. PM Period</li>
\t\t\t\t\t\t<li>16:00 p.m. Closing of PM</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row copyright\">
\t\t\t\t<div class=\"col-md-12 text-center\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<small class=\"block\">&copy; 2019 SJSSS. All Rights Reserved.</small> 
\t\t\t\t\t\t<small class=\"block\">Designed by The Students.</small>
\t\t\t\t\t</p>
\t\t\t\t\t<p>
\t\t\t\t\t\t<ul class=\"fh5co-social-icons\">
\t\t\t\t\t\t\t<li><a
\t\t\t\t\t\t\t\t\thref=\"https://www.facebook.com/Saint-Josephs-Senior-Secondary-School-2481936115172108/?modal=admin_todo_tour\"
\t\t\t\t\t\t\t\t\ttarget=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-facebook\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"https://www.youtube.com/channel/UC3nddtNh66EKLt_gLxKsb6w\" target=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-youtube\"></i></a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>
\t</footer>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 21,  68 => 20,  64 => 19,  60 => 18,  56 => 17,  52 => 16,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer id=\"fh5co-footer\" role=\"contentinfo\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row row-pb-md\">
\t\t\t\t<div class=\"col-md-4 fh5co-widget\">
\t\t\t\t\t<h4>St. Joseph's Sen. Sec. Sch.</h4>
\t\t\t\t\t<ul class=\"fh5co-footer-links\">
\t\t\t\t\t\t<li>P.O Box 119</li>
\t\t\t\t\t\t<li>Box Bar Road.</li>
\t\t\t\t\t\t<li>+220 ‭422-7709</li>
\t\t\t\t\t\t<li>stjosephs1921@yahoo.co.uk</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 col-sm-4 col-xs-6 col-md-push-1\">
\t\t\t\t\t<h4>Work &amp; Pray</h4>
\t\t\t\t\t<ul class=\"fh5co-footer-links\">
\t\t\t\t\t\t<li><a href=\"{{ 'home'|page}}\">Home</a></li>
\t\t\t\t\t\t<li><a href=\"{{ 'about'|page}}\">About</a></li>
\t\t\t\t\t\t<li><a href=\"{{ 'events'|page }}\">Events</a></li>
\t\t\t\t\t\t<li><a href=\"{{ 'blog'|page }}\">Blog</a></li>
\t\t\t\t\t\t<li><a href=\"{{ 'gallery'|page }}\">Gallery</a></li>
\t\t\t\t\t\t<li><a href=\"{{ 'contact'|page }}\">Contact</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-4 col-sm-4 col-xs-6 col-md-push-1\">
\t\t\t\t\t<h4>School Hours</h4>
\t\t\t\t\t<ul class=\"fh5co-footer-links\">
\t\t\t\t\t\t<li>8:15 a.m. Classes Begin</li>
\t\t\t\t\t\t<li>10:55 a.m. Break Period</li>
\t\t\t\t\t\t<li>11:30 a.m. Classes Begin</li>
\t\t\t\t\t\t<li>13:55 p.m. Closing Period</li>
\t\t\t\t\t\t<li>14:15 p.m. PM Period</li>
\t\t\t\t\t\t<li>16:00 p.m. Closing of PM</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row copyright\">
\t\t\t\t<div class=\"col-md-12 text-center\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<small class=\"block\">&copy; 2019 SJSSS. All Rights Reserved.</small> 
\t\t\t\t\t\t<small class=\"block\">Designed by The Students.</small>
\t\t\t\t\t</p>
\t\t\t\t\t<p>
\t\t\t\t\t\t<ul class=\"fh5co-social-icons\">
\t\t\t\t\t\t\t<li><a
\t\t\t\t\t\t\t\t\thref=\"https://www.facebook.com/Saint-Josephs-Senior-Secondary-School-2481936115172108/?modal=admin_todo_tour\"
\t\t\t\t\t\t\t\t\ttarget=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-facebook\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"https://www.youtube.com/channel/UC3nddtNh66EKLt_gLxKsb6w\" target=\"_blank\"><i
\t\t\t\t\t\t\t\t\t\tclass=\"icon-youtube\"></i></a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>
\t</footer>", "/Users/user/Sites/stjosephsss/themes/st-josephs/partials/footer.htm", "");
    }
}
