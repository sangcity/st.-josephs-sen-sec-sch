<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/blog.htm */
class __TwigTemplate_1fbcf02c4196b2b83ff898421092ae54eac550cc72a1a67a969d8a86658a8a6e extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\"
\t\tstyle=\"background-image:url(";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/picture.jpg");
        echo ");\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Blog</h1>
\t\t\t\t\t\t\t<h2>We are an all-girl school. Thus we're proud.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>

\t<div id=\"fh5co-blog\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2>Latest Post</h2>
\t\t\t\t\t<p>With the help of the our weekly Thursday Assemblies we have notured a routine of a creativity, academics and entertainment.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 27
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, false, 27);
        // line 28
        echo "\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 29
            echo "\t\t\t\t\t<div class=\"col-lg-4 col-md-4\">
\t\t\t\t\t\t<div class=\"fh5co-blog animate-box\">
\t\t\t\t\t\t\t";
            // line 31
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, false, 31), "count", [], "any", false, false, false, 31)) {
                echo "         
\t                ";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, false, 32));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 33
                    echo "\t                <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 33), "html", null, true);
                    echo "\"><img class=\"img-responsive\"  data-src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "filename", [], "any", false, false, false, 33), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 33), "html", null, true);
                    echo "\"
\t                    alt=\"";
                    // line 34
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "description", [], "any", false, false, false, 34), "html", null, true);
                    echo "\"></a>
\t                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                echo "         
\t\t\t\t\t\t\t ";
            }
            // line 37
            echo "\t\t\t\t\t\t\t<div class=\"blog-text\">
\t\t\t\t\t\t\t\t<h3><i class=\"icon-open-book blog-icons\"></i><a
\t\t\t\t\t\t\t\t\t\thref=\"";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 39), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 39), "html", null, true);
            echo "</a></h3>
\t\t\t\t\t\t\t\t<span class=\"posted_on\">";
            // line 40
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "published_at", [], "any", false, false, false, 40), "M d, Y"), "html", null, true);
            echo "</span>
\t\t\t\t\t\t\t\t<span class=\"posted_on\">Posted
\t\t\t\t\t\t\t\t\t";
            // line 42
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "categories", [], "any", false, false, false, 42), "count", [], "any", false, false, false, 42)) {
                echo " in ";
            }
            // line 43
            echo "\t\t\t\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "categories", [], "any", false, false, false, 43));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 44
                echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "url", [], "any", false, false, false, 44), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 44), "html", null, true);
                echo "</a>";
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 44)) {
                    echo ", ";
                }
                // line 45
                echo "\t\t\t\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<p>";
            // line 47
            echo call_user_func_array($this->env->getFunction('str_limit')->getCallable(), ["limit", twig_get_attribute($this->env, $this->source, $context["post"], "summary", [], "any", false, false, false, 47), 120]);
            echo "</p>
\t\t\t\t\t\t\t\t<a href=\"";
            // line 48
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 48), "html", null, true);
            echo "\" class=\"btn btn-primary\">Read More</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 53,  173 => 48,  169 => 47,  166 => 46,  152 => 45,  143 => 44,  125 => 43,  121 => 42,  116 => 40,  110 => 39,  106 => 37,  102 => 35,  94 => 34,  85 => 33,  81 => 32,  77 => 31,  73 => 29,  68 => 28,  66 => 27,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\"
\t\tstyle=\"background-image:url({{ 'assets/images/picture.jpg'|theme }});\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Blog</h1>
\t\t\t\t\t\t\t<h2>We are an all-girl school. Thus we're proud.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>

\t<div id=\"fh5co-blog\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2>Latest Post</h2>
\t\t\t\t\t<p>With the help of the our weekly Thursday Assemblies we have notured a routine of a creativity, academics and entertainment.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t{% set posts = blogPosts.posts %}
\t\t\t\t{% for post in posts %}
\t\t\t\t\t<div class=\"col-lg-4 col-md-4\">
\t\t\t\t\t\t<div class=\"fh5co-blog animate-box\">
\t\t\t\t\t\t\t{% if post.featured_images.count %}         
\t                {% for image in post.featured_images %}
\t                <a href=\"{{ post.url }}\"><img class=\"img-responsive\"  data-src=\"{{ image.filename }}\" src=\"{{ image.path }}\"
\t                    alt=\"{{ image.description }}\"></a>
\t                {% endfor %}         
\t\t\t\t\t\t\t {% endif %}
\t\t\t\t\t\t\t<div class=\"blog-text\">
\t\t\t\t\t\t\t\t<h3><i class=\"icon-open-book blog-icons\"></i><a
\t\t\t\t\t\t\t\t\t\thref=\"{{ post.url }}\">{{ post.title }}</a></h3>
\t\t\t\t\t\t\t\t<span class=\"posted_on\">{{ post.published_at|date('M d, Y') }}</span>
\t\t\t\t\t\t\t\t<span class=\"posted_on\">Posted
\t\t\t\t\t\t\t\t\t{% if post.categories.count %} in {% endif %}
\t\t\t\t\t\t\t\t\t{% for category in post.categories %}
\t\t\t\t\t\t\t\t\t<a href=\"{{ category.url }}\">{{ category.name }}</a>{% if not loop.last %}, {% endif %}
\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<p>{{ str_limit(post.summary|raw, 120) }}</p>
\t\t\t\t\t\t\t\t<a href=\"{{ post.url }}\" class=\"btn btn-primary\">Read More</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t{% endfor %}
\t\t\t</div>
\t\t</div>
\t</div>
</div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/blog.htm", "");
    }
}
