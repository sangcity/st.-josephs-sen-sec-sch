<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/contact.htm */
class __TwigTemplate_b292700de1f0e86a070cb2cb6f8332d54c49eb5518033441d560abe5e9449526 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\"
\t\tstyle=\"background-image:url(";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/picture.jpg");
        echo ");\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Contact Us</h1>
\t\t\t\t\t\t\t<h2>We are an all-girl school. Thus we're proud.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>
\t<div id=\"fh5co-contact\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<iframe id=\"map\" class=\"fh5co-map\"
\t\t\t\t\tsrc=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3880.281585965462!2d-16.585890085172732!3d13.45673189053891!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec29dc28df1f689%3A0xce437469726e3499!2sSt+Joseph&#39;s+Senior+Secondary+School!5e0!3m2!1sen!2sgm!4v1561491017849!5m2!1sen!2sgm\"
\t\t\t\t\twidth=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen>
\t\t\t\t</iframe>
\t\t\t\t<div class=\"col-md-5 col-md-push-1 animate-box\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"fh5co-contact-info\">
\t\t\t\t\t\t<h3>Contact Information</h3>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li class=\"address\">P.O Box 119,<br>Box Bar Road.</li>
\t\t\t\t\t\t\t<li class=\"phone\"><a href=\"tel://1234567920\">+220 ‭422-7709</a></li>
\t\t\t\t\t\t\t<li class=\"email\"><a href=\"mailto:stjosephs1921@yahoo.co.uk\">stjosephs1921@yahoo.co.uk</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 animate-box  text-center pt-40\">
\t\t\t\t\t<h3>Get In Touch</h3>
\t\t\t\t\t<div class=\"col-md-12 animate-box\">
\t\t\t\t\t\t<p><a class=\"btn btn-primary btn-lg btn-learn\" href=\"mailto:stjosephs1921@yahoo.co.uk\" style=\"width: 70%;\">Shoot An Email Us</a></p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>
\t</div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\"
\t\tstyle=\"background-image:url({{ 'assets/images/picture.jpg'|theme }});\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Contact Us</h1>
\t\t\t\t\t\t\t<h2>We are an all-girl school. Thus we're proud.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>
\t<div id=\"fh5co-contact\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<iframe id=\"map\" class=\"fh5co-map\"
\t\t\t\t\tsrc=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3880.281585965462!2d-16.585890085172732!3d13.45673189053891!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec29dc28df1f689%3A0xce437469726e3499!2sSt+Joseph&#39;s+Senior+Secondary+School!5e0!3m2!1sen!2sgm!4v1561491017849!5m2!1sen!2sgm\"
\t\t\t\t\twidth=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen>
\t\t\t\t</iframe>
\t\t\t\t<div class=\"col-md-5 col-md-push-1 animate-box\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"fh5co-contact-info\">
\t\t\t\t\t\t<h3>Contact Information</h3>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li class=\"address\">P.O Box 119,<br>Box Bar Road.</li>
\t\t\t\t\t\t\t<li class=\"phone\"><a href=\"tel://1234567920\">+220 ‭422-7709</a></li>
\t\t\t\t\t\t\t<li class=\"email\"><a href=\"mailto:stjosephs1921@yahoo.co.uk\">stjosephs1921@yahoo.co.uk</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 animate-box  text-center pt-40\">
\t\t\t\t\t<h3>Get In Touch</h3>
\t\t\t\t\t<div class=\"col-md-12 animate-box\">
\t\t\t\t\t\t<p><a class=\"btn btn-primary btn-lg btn-learn\" href=\"mailto:stjosephs1921@yahoo.co.uk\" style=\"width: 70%;\">Shoot An Email Us</a></p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>
\t</div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/contact.htm", "");
    }
}
