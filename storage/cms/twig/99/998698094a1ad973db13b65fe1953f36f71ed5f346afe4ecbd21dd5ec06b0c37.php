<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/blog-details.htm */
class __TwigTemplate_26f67020ec57cedf761877a5fdb9447df9a99e764c59a344658b40ee15769c6f extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"fh5co-header\" class=\"blog-details-header fh5co-cover fh5co-cover-sm\" role=\"banner\"
      style=\"background-image:url(";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/picture.jpg");
        echo ");\" data-stellar-background-ratio=\"0.5\">
      <div class=\"overlay\"></div>
</header>

    <div id=\"fh5co-blog\">
      <div class=\"container\">

      ";
        // line 9
        $context["post"] = twig_get_attribute($this->env, $this->source, ($context["blogPost"] ?? null), "post", [], "any", false, false, false, 9);
        // line 10
        echo "
        <div class=\"row\">
          <div class=\"col-lg-8 col-md-8\">
            <div class=\"fh5co-blog animate-box\">
              <a href=\"#\">
                \t";
        // line 15
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, false, 15), "count", [], "any", false, false, false, 15)) {
            // line 16
            echo "                \t             
                \t  ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, false, 17));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 18
                echo "                \t    <a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, false, 18), "html", null, true);
                echo "\"><img class=\"img-responsive text-center\" data-src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "filename", [], "any", false, false, false, 18), "html", null, true);
                echo "\"
                \t    src=\"";
                // line 19
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 19), "html", null, true);
                echo "\"  alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "description", [], "any", false, false, false, 19), "html", null, true);
                echo "\"></a>
                \t  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "                \t             
                  ";
        }
        // line 23
        echo "
                </a>
              <div class=\"col-lg-12 blog-text\" id=\"blog-details\">
                <div class=\"col-lg-12\">
                  <h3><i class=\"icon-open-book blog-icons\"></i><a href=\"\"
                        #>";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 28), "html", null, true);
        echo "</a></h3>
                  <span class=\"posted_on\">";
        // line 29
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, false, 29), "M d, Y"), "html", null, true);
        echo "</span>
                  <!-- <span class=\"comment\"><a href=\"\">21<i class=\"icon-speech-bubble\"></i></a></span> -->
                  <p>";
        // line 31
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "content_html", [], "any", false, false, false, 31);
        echo "
                  </p>
                </div>
            </div>
          </div>";
        // line 35
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("disqus"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 36
        echo "        </div>
        <div class=\"col-lg-4 col-md-4 mt-40\"
          style=\"border: 1px solid #fff; border-radius: 10px;  box-shadow: 0 1px 3px 0 rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 2px 1px -1px rgba(0,0,0,.12);
    -moz-box-shadow: 0px 10px 20px -12px rgba(0, 0, 0, 0.10);
    box-shadow: 0px 10px 20px -12px rgba(0, 0, 0, 0.10)\">
          <div class=\"fh5co-blog animate-box blog-text\">
            <div class=\"animate-box\">
              <h3 class=\"mb-20 ml-40 pt-30\">More Articles.</h3>
            </div>
            \t<ul class=\"list-nav\">
                 \t";
        // line 46
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, false, 46);
        // line 47
        echo "                   ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 48
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 48) < 5)) {
                // line 49
                echo "                      <a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 49), "html", null, true);
                echo "\">
                        <h4><i class=\"icon-open-book blog-icons\"></i>";
                // line 50
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 50), "html", null, true);
                echo "</h4>
                      </a>
                      <p>";
                // line 52
                echo call_user_func_array($this->env->getFunction('html_limit')->getCallable(), ["limit", twig_get_attribute($this->env, $this->source, $context["post"], "summary", [], "any", false, false, false, 52), 125]);
                echo "</p>
                    ";
            }
            // line 54
            echo "                  ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "\t\t\t\t\t\t\t</ul>
          </div>
        </div>
      </div>
              
        </div>
      </div>
    </div>
  </div>
  </div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/blog-details.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 55,  164 => 54,  159 => 52,  154 => 50,  149 => 49,  146 => 48,  128 => 47,  126 => 46,  114 => 36,  110 => 35,  103 => 31,  98 => 29,  94 => 28,  87 => 23,  83 => 21,  73 => 19,  66 => 18,  62 => 17,  59 => 16,  57 => 15,  50 => 10,  48 => 9,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"fh5co-header\" class=\"blog-details-header fh5co-cover fh5co-cover-sm\" role=\"banner\"
      style=\"background-image:url({{ 'assets/images/picture.jpg'|theme }});\" data-stellar-background-ratio=\"0.5\">
      <div class=\"overlay\"></div>
</header>

    <div id=\"fh5co-blog\">
      <div class=\"container\">

      {% set post = blogPost.post %}

        <div class=\"row\">
          <div class=\"col-lg-8 col-md-8\">
            <div class=\"fh5co-blog animate-box\">
              <a href=\"#\">
                \t{% if post.featured_images.count %}
                \t             
                \t  {% for image in post.featured_images %}
                \t    <a href=\"{{ post.url }}\"><img class=\"img-responsive text-center\" data-src=\"{{ image.filename }}\"
                \t    src=\"{{ image.path }}\"  alt=\"{{ image.description }}\"></a>
                \t  {% endfor %}
                \t             
                  {% endif %}

                </a>
              <div class=\"col-lg-12 blog-text\" id=\"blog-details\">
                <div class=\"col-lg-12\">
                  <h3><i class=\"icon-open-book blog-icons\"></i><a href=\"\"
                        #>{{ post.title }}</a></h3>
                  <span class=\"posted_on\">{{ post.published_at|date('M d, Y') }}</span>
                  <!-- <span class=\"comment\"><a href=\"\">21<i class=\"icon-speech-bubble\"></i></a></span> -->
                  <p>{{ post.content_html|raw }}
                  </p>
                </div>
            </div>
          </div>{% component 'disqus' %}
        </div>
        <div class=\"col-lg-4 col-md-4 mt-40\"
          style=\"border: 1px solid #fff; border-radius: 10px;  box-shadow: 0 1px 3px 0 rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 2px 1px -1px rgba(0,0,0,.12);
    -moz-box-shadow: 0px 10px 20px -12px rgba(0, 0, 0, 0.10);
    box-shadow: 0px 10px 20px -12px rgba(0, 0, 0, 0.10)\">
          <div class=\"fh5co-blog animate-box blog-text\">
            <div class=\"animate-box\">
              <h3 class=\"mb-20 ml-40 pt-30\">More Articles.</h3>
            </div>
            \t<ul class=\"list-nav\">
                 \t{% set posts = blogPosts.posts %}
                   {% for post in posts %}
                    {% if loop.index < 5 %}
                      <a href=\"{{ post.url }}\">
                        <h4><i class=\"icon-open-book blog-icons\"></i>{{ post.title }}</h4>
                      </a>
                      <p>{{ html_limit(post.summary|raw, 125) }}</p>
                    {% endif %}
                  {% endfor %}
\t\t\t\t\t\t\t</ul>
          </div>
        </div>
      </div>
              
        </div>
      </div>
    </div>
  </div>
  </div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/blog-details.htm", "");
    }
}
