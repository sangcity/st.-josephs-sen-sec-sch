<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/home.htm */
class __TwigTemplate_110690e14c1446c72446439bc1372a0d3bff10fd4d59ad7055e4bf61d6adc3c5 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 3);
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 4);
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 5);
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 6);
        // line 7
        echo "
\t";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 9
            echo "\t\t<header id=\"fh5co-header\" class=\"fh5co-cover\" role=\"banner\"
\t\t\tstyle=\"background-image:url(";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "banner_img", [], "any", false, false, false, 10), "path", [], "any", false, false, false, 10), "html", null, true);
            echo ");\" data-stellar-background-ratio=\"0.5\">
\t\t\t\t<div class=\"overlay\"></div>
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t\t\t<h1>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 17), "html", null, true);
            echo "</h1>
\t\t\t\t\t\t\t\t\t<p style=\"color: #fff; font-size: 20px;\">";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 18), "html", null, true);
            echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</header>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "\t<div id=\"fh5co-counter\" class=\"fh5co-counters\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span class=\"fh5co-counter js-counter\" data-from=\"0\" data-to=\"598\" data-speed=\"50\" data-refresh-interval=\"50\"></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Students</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span class=\"fh5co-counter js-counter\" data-from=\"0\" data-to=\"24\" data-speed=\"50\" data-refresh-interval=\"50\"></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Subjects</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span class=\"fh5co-counter js-counter\" data-from=\"0\" data-to=\"33\" data-speed=\"50\" data-refresh-interval=\"50\"></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Teachers</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"fh5co-explore\" class=\"fh5co-bg-section\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2>About</h2>
\t\t\t\t\t<p>  We have gone through so much for almost a century, browse through our history, know us more.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>\t\t
\t\t<div class=\"fh5co-explore fh5co-explore1\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6 col-md-push-6 animate-box\">
\t\t\t\t\t\t\t\t<!-- <div class=\"home-player-overlay\"></div> -->
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"";
        // line 59
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/Abt-frontpage.jpg");
        echo "\" alt=\"work\" class=\"z-index: 1;\">
\t\t\t\t\t\t<!-- <a class=\"icon-play\" href=\"https://www.youtube.com/watch?v=GjFcq0DSunE\" target=\"_blank\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</a> -->
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 col-md-pull-6 animate-box\">
\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t<h3>We Are The Great St. Josephs</h3>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\tSince 1883, the Sisters of St. Joseph of Cluny have worked in The Gambia with devotion and dedication in the
\t\t\t\t\t\t\t\tfields of Education, Nursing and other social work for the promotion of the full development of the Gambian
\t\t\t\t\t\t\t\tpeople.
\t\t\t\t\t\t\t\tSt. Joseph’s Senior Secondary School (previously St. Joseph’s High School) was established in 1921 as a Roman
\t\t\t\t\t\t\t\tCatholic Mission School for Girls by the Congregation with an initial enrolment of forty-four students headed by
\t\t\t\t\t\t\t\tMrs. Nellie Morris who handed over to Sr. Celeste Wall in 1923. The school’s policy was to instil self
\t\t\t\t\t\t\t\tdiscipline, inculcate moral values and to provide a model education for its students, Christians and Muslims
\t\t\t\t\t\t\t\talike; with basic reading, writing, arithmetic, music (introduced in 1926), home economics skills with a great
\t\t\t\t\t\t\t\temphasis on religious education. 
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<div class=\"col-md-12 animate-box\">
\t\t\t\t\t\t\t<p><a class=\"btn btn-primary btn-lg btn-learn\" href=\"";
        // line 79
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("about");
        echo "\">Read More</a></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container\">
\t\t\t<div class=\"pricing\">
\t\t\t\t\t<div class=\"mt animate-box\">
\t\t\t\t\t\t<h3 class=\"mb-50\">Our Students Are Offered.</h3>
\t\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t<h2 class=\"pricing-plan\">Commerce</h2>
\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t<li>Accounting</li>
\t\t\t\t\t\t\t<li class=\"color\">Economics</li>
\t\t\t\t\t\t\t<li>Business Management</li>
\t\t\t\t\t\t\t<li class=\"color\">Agricultural Science</li>
\t\t\t\t\t\t\t<li>Literature</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t<h2 class=\"pricing-plan\">Science<span>Art Science</span></h2>
\t\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t\t<li>Chemistry</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Physics</li>
\t\t\t\t\t\t\t\t<li>Biology</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Further Mathematics</li>
\t\t\t\t\t\t\t\t<li>Visual Arts</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Geography</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box popular\">
\t\t\t\t\t\t<h2 class=\"pricing-plan pricing-plan-offer\">Science<span>Pure Science</span></h2>
\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t<li>Chemistry</li>
\t\t\t\t\t\t\t<li class=\"color\">Physics</li>
\t\t\t\t\t\t\t<li>Biology</li>
\t\t\t\t\t\t\t<li class=\"color\">Further Mathematics</li>
\t\t\t\t\t\t\t<li>Visual Arts</li>
\t\t\t\t\t\t\t<li class=\"color\">Geography</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t<h2 class=\"pricing-plan\">Arts</h2>
\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t<li>History</li>
\t\t\t\t\t\t\t<li class=\"color\">Government</li>
\t\t\t\t\t\t\t<li>Biology</li>
\t\t\t\t\t\t\t<li class=\"color\">General Science</li>
\t\t\t\t\t\t\t<li>Religious Studies</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t</div>
\t\t</div>
\t\t</div>
\t</div>

\t\t\t<div id=\"fh5co-counter\" class=\"fh5co-counters\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12 text-center animate-box\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalScrollable\" style=\"width: 25%; font-size: 22px;\">
\t\t\t\t\t\t\t\t\t\tRegister Now
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- Button trigger modal -->
\t
\t\t\t<!-- Modal -->
\t\t\t<div class=\"modal fade\" id=\"exampleModalScrollable\" tabindex=\"-1\" role=\"dialog\"
\t\t\t\taria-labelledby=\"exampleModalScrollableTitle\" aria-hidden=\"true\">
\t\t\t\t<div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">
\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalScrollableTitle\">All About Our History</h5>
\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 animate-box\">
\t\t\t\t\t\t\t\t\t\t<h3>Get In Touch</h3>
\t\t\t\t\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"fname\">First Name</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"fname\" class=\"form-control\" placeholder=\"Your firstname\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"lname\">Last Name</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"lname\" class=\"form-control\" placeholder=\"Your lastname\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"email\">Email</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"email\" class=\"form-control\" placeholder=\"Your email address\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"subject\">Subject</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"subject\" class=\"form-control\" placeholder=\"Your subject of this message\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"message\">Message</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"message\" id=\"message\" cols=\"30\" rows=\"10\" class=\"form-control\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Say something about us\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Send Message\" class=\"btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t</div>
\t\t<div id=\"fh5co-counter\" class=\"fh5co-counters\" style=\"background-color: #3e64ef38;\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t<h2>Achievements</h2>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-4 text-center\">
\t\t\t\t\t\t<span id=\"icons\" class=\"icon-tv\"></span>
\t\t\t\t\t\t<spa>
\t\t\t\t\t\t\t<span class=\"fh5co-counter-label mt-40 animate-box\">A school website</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4 text-center\">
\t\t\t\t\t\t<span id=\"icons\" class=\"icon-v-card\"></span>
\t\t\t\t\t\t<span class=\"fh5co-counter-label mt-40 animate-box\">Generate E-Report Cards for students</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4 text-center\">
\t\t\t\t\t\t<span id=\"icons\" class=\"icon-open-book\"></span>
\t\t\t\t\t\t<span class=\"fh5co-counter-label mt-40 animate-box\">A digitized library</span>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t<div id=\"fh5co-testimonial\" class=\"fh5co-bg-section\" style=\"background: #efefef;\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t<h2>Recent Post</h2>
\t\t\t\t\t<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab
\t\t\t\t\t\taliquam dolor eius.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t";
        // line 269
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, false, 269);
        // line 270
        echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
        // line 271
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 272
            echo "\t\t\t\t\t\t\t\t\t";
            if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 272) < 5)) {
                // line 273
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"fh5co-blog animate-box\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 275
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, false, 275), "count", [], "any", false, false, false, 275)) {
                    echo "         
\t\t\t\t\t\t\t\t\t\t\t\t                ";
                    // line 276
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, false, 276));
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        // line 277
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t                <a href=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 277), "html", null, true);
                        echo "\"><img class=\"img-responsive\" data-src=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "filename", [], "any", false, false, false, 277), "html", null, true);
                        echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\tsrc=\"";
                        // line 278
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [0 => 350, 1 => 247, 2 => ["mode" => "crop"]], "method", false, false, false, 278), "html", null, true);
                        echo "\"                     alt=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "description", [], "any", false, false, false, 278), "html", null, true);
                        echo "\"></a>
\t\t\t\t\t\t\t\t\t\t\t\t                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 279
                    echo "         
\t\t\t\t\t\t\t\t\t\t\t\t ";
                }
                // line 281
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"blog-text blog-text-home\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h3><a href=\"";
                // line 282
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 282), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 282), "html", null, true);
                echo "</a></h3>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"posted_on\">";
                // line 283
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "published_at", [], "any", false, false, false, 283), "M d, Y"), "html", null, true);
                echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"posted_on\">Posted
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 285
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "categories", [], "any", false, false, false, 285), "count", [], "any", false, false, false, 285)) {
                    echo " in ";
                }
                // line 286
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "categories", [], "any", false, false, false, 286));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 287
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "url", [], "any", false, false, false, 287), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 287), "html", null, true);
                    echo "</a>";
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 287)) {
                        echo ", ";
                    }
                    // line 288
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 289
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>";
                // line 290
                echo call_user_func_array($this->env->getFunction('html_limit')->getCallable(), ["limit", twig_get_attribute($this->env, $this->source, $context["post"], "summary", [], "any", false, false, false, 290), 120]);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 291
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, false, 291), "html", null, true);
                echo "\" class=\"btn btn-primary\">Read More</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            // line 296
            echo "\t\t\t\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 297
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"fh5co-blog\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t<h2>Gallery</h2>
\t\t\t\t\t<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t\t<div class=\"container-fluid proj-bottom\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t";
        // line 311
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["gallery"] ?? null), "records", [], "any", false, false, false, 311);
        // line 312
        echo "\t\t\t\t\t";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["gallery"] ?? null), "displayColumn", [], "any", false, false, false, 312);
        // line 313
        echo "\t\t\t\t\t";
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["gallery"] ?? null), "noRecordsMessage", [], "any", false, false, false, 313);
        // line 314
        echo "\t\t\t\t\t";
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["gallery"] ?? null), "detailsPage", [], "any", false, false, false, 314);
        // line 315
        echo "\t\t\t\t\t";
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["gallery"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 315);
        // line 316
        echo "\t\t\t\t\t";
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["gallery"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 316);
        // line 317
        echo "
\t\t\t\t\t";
        // line 318
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 319
            echo "
\t\t\t\t\t";
            // line 320
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["record"], "gallery", [], "any", false, false, false, 320));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 321
                echo "
\t\t\t\t\t";
                // line 322
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 322) < 9)) {
                    // line 323
                    echo "
\t\t\t\t\t\t<div class=\"col-md-4 col-sm-6 col-lg-3 fh5co-project animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t<img src=\"";
                    // line 326
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [0 => 392, 1 => 261, 2 => ["mode" => "crop"]], "method", false, false, false, 326), "html", null, true);
                    echo "\" alt=\"Free HTML5 Website Template by FreeHTML5.co\"
\t\t\t\t\t\t\t\t\tclass=\"img-responsive\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>

\t\t\t\t\t";
                }
                // line 332
                echo "
\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 334
            echo "
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 336
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12 text-center animate-box\">
\t\t\t\t\t\t<p><a class=\"btn btn-primary btn-lg btn-learn\" href=\"";
        // line 339
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("gallery");
        echo "\">View More</a></p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t</div>

\t<div id=\"fh5co-started\" style=\"background-image:url(images/schLocations.jpg);\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t<h2>Get Started with SJSSS</h2>
\t\t\t\t\t<p>The St. Joseph’s Experience is very powerful</p>
\t\t\t\t\t<p>It gives students a unique sense of boldness. <br>
\t\t\t\t\t\tIt helps them to develop their God given talents and potentials. <br>
\t\t\t\t\t\tIt gives the girls the opportunity to explore different disciplines and integrate fields of knowledge,
\t\t\t\t\t\tand, to ask the right questions. <br>
\t\t\t\t\t\tHere students are uniquely prepared to not only fit in but to excel wherever
\t\t\t\t\t\tthey may find themselves.
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<p><a href=\"";
        // line 362
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\" class=\"btn btn-default btn-lg\">Contact Us</a></p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  614 => 362,  588 => 339,  583 => 336,  576 => 334,  561 => 332,  552 => 326,  547 => 323,  545 => 322,  542 => 321,  525 => 320,  522 => 319,  518 => 318,  515 => 317,  512 => 316,  509 => 315,  506 => 314,  503 => 313,  500 => 312,  498 => 311,  482 => 297,  468 => 296,  460 => 291,  456 => 290,  453 => 289,  439 => 288,  430 => 287,  412 => 286,  408 => 285,  403 => 283,  397 => 282,  394 => 281,  390 => 279,  380 => 278,  373 => 277,  369 => 276,  365 => 275,  361 => 273,  358 => 272,  341 => 271,  338 => 270,  336 => 269,  143 => 79,  120 => 59,  85 => 26,  71 => 18,  67 => 17,  57 => 10,  54 => 9,  50 => 8,  47 => 7,  45 => 6,  43 => 5,  41 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

\t{% for record in records %}
\t\t<header id=\"fh5co-header\" class=\"fh5co-cover\" role=\"banner\"
\t\t\tstyle=\"background-image:url({{ record.banner_img.path  }});\" data-stellar-background-ratio=\"0.5\">
\t\t\t\t<div class=\"overlay\"></div>
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t\t\t<h1>{{ record.name }}</h1>
\t\t\t\t\t\t\t\t\t<p style=\"color: #fff; font-size: 20px;\">{{ record.description }}</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</header>
\t{% endfor %}
\t<div id=\"fh5co-counter\" class=\"fh5co-counters\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span class=\"fh5co-counter js-counter\" data-from=\"0\" data-to=\"598\" data-speed=\"50\" data-refresh-interval=\"50\"></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Students</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span class=\"fh5co-counter js-counter\" data-from=\"0\" data-to=\"24\" data-speed=\"50\" data-refresh-interval=\"50\"></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Subjects</span>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4 text-center animate-box\">
\t\t\t\t\t<span class=\"fh5co-counter js-counter\" data-from=\"0\" data-to=\"33\" data-speed=\"50\" data-refresh-interval=\"50\"></span>
\t\t\t\t\t<span class=\"fh5co-counter-label\">Teachers</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"fh5co-explore\" class=\"fh5co-bg-section\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t<h2>About</h2>
\t\t\t\t\t<p>  We have gone through so much for almost a century, browse through our history, know us more.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>\t\t
\t\t<div class=\"fh5co-explore fh5co-explore1\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6 col-md-push-6 animate-box\">
\t\t\t\t\t\t\t\t<!-- <div class=\"home-player-overlay\"></div> -->
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"{{ 'assets/images/Abt-frontpage.jpg'|theme }}\" alt=\"work\" class=\"z-index: 1;\">
\t\t\t\t\t\t<!-- <a class=\"icon-play\" href=\"https://www.youtube.com/watch?v=GjFcq0DSunE\" target=\"_blank\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t</a> -->
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 col-md-pull-6 animate-box\">
\t\t\t\t\t\t<div class=\"mt\">
\t\t\t\t\t\t\t<h3>We Are The Great St. Josephs</h3>
\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\tSince 1883, the Sisters of St. Joseph of Cluny have worked in The Gambia with devotion and dedication in the
\t\t\t\t\t\t\t\tfields of Education, Nursing and other social work for the promotion of the full development of the Gambian
\t\t\t\t\t\t\t\tpeople.
\t\t\t\t\t\t\t\tSt. Joseph’s Senior Secondary School (previously St. Joseph’s High School) was established in 1921 as a Roman
\t\t\t\t\t\t\t\tCatholic Mission School for Girls by the Congregation with an initial enrolment of forty-four students headed by
\t\t\t\t\t\t\t\tMrs. Nellie Morris who handed over to Sr. Celeste Wall in 1923. The school’s policy was to instil self
\t\t\t\t\t\t\t\tdiscipline, inculcate moral values and to provide a model education for its students, Christians and Muslims
\t\t\t\t\t\t\t\talike; with basic reading, writing, arithmetic, music (introduced in 1926), home economics skills with a great
\t\t\t\t\t\t\t\temphasis on religious education. 
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<div class=\"col-md-12 animate-box\">
\t\t\t\t\t\t\t<p><a class=\"btn btn-primary btn-lg btn-learn\" href=\"{{ 'about'|page }}\">Read More</a></p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"container\">
\t\t\t<div class=\"pricing\">
\t\t\t\t\t<div class=\"mt animate-box\">
\t\t\t\t\t\t<h3 class=\"mb-50\">Our Students Are Offered.</h3>
\t\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t<h2 class=\"pricing-plan\">Commerce</h2>
\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t<li>Accounting</li>
\t\t\t\t\t\t\t<li class=\"color\">Economics</li>
\t\t\t\t\t\t\t<li>Business Management</li>
\t\t\t\t\t\t\t<li class=\"color\">Agricultural Science</li>
\t\t\t\t\t\t\t<li>Literature</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t<h2 class=\"pricing-plan\">Science<span>Art Science</span></h2>
\t\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t\t<li>Chemistry</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Physics</li>
\t\t\t\t\t\t\t\t<li>Biology</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Further Mathematics</li>
\t\t\t\t\t\t\t\t<li>Visual Arts</li>
\t\t\t\t\t\t\t\t<li class=\"color\">Geography</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box popular\">
\t\t\t\t\t\t<h2 class=\"pricing-plan pricing-plan-offer\">Science<span>Pure Science</span></h2>
\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t<li>Chemistry</li>
\t\t\t\t\t\t\t<li class=\"color\">Physics</li>
\t\t\t\t\t\t\t<li>Biology</li>
\t\t\t\t\t\t\t<li class=\"color\">Further Mathematics</li>
\t\t\t\t\t\t\t<li>Visual Arts</li>
\t\t\t\t\t\t\t<li class=\"color\">Geography</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-3 animate-box\">
\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t<h2 class=\"pricing-plan\">Arts</h2>
\t\t\t\t\t\t<ul class=\"classes\">
\t\t\t\t\t\t\t<li>English</li>
\t\t\t\t\t\t\t<li class=\"color\">Maths</li>
\t\t\t\t\t\t\t<li>History</li>
\t\t\t\t\t\t\t<li class=\"color\">Government</li>
\t\t\t\t\t\t\t<li>Biology</li>
\t\t\t\t\t\t\t<li class=\"color\">General Science</li>
\t\t\t\t\t\t\t<li>Religious Studies</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t</div>
\t\t</div>
\t\t</div>
\t</div>

\t\t\t<div id=\"fh5co-counter\" class=\"fh5co-counters\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12 text-center animate-box\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalScrollable\" style=\"width: 25%; font-size: 22px;\">
\t\t\t\t\t\t\t\t\t\tRegister Now
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- Button trigger modal -->
\t
\t\t\t<!-- Modal -->
\t\t\t<div class=\"modal fade\" id=\"exampleModalScrollable\" tabindex=\"-1\" role=\"dialog\"
\t\t\t\taria-labelledby=\"exampleModalScrollableTitle\" aria-hidden=\"true\">
\t\t\t\t<div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">
\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t<h5 class=\"modal-title\" id=\"exampleModalScrollableTitle\">All About Our History</h5>
\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 animate-box\">
\t\t\t\t\t\t\t\t\t\t<h3>Get In Touch</h3>
\t\t\t\t\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"fname\">First Name</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"fname\" class=\"form-control\" placeholder=\"Your firstname\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"lname\">Last Name</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"lname\" class=\"form-control\" placeholder=\"Your lastname\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"email\">Email</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"email\" class=\"form-control\" placeholder=\"Your email address\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"subject\">Subject</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"subject\" class=\"form-control\" placeholder=\"Your subject of this message\">
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div class=\"row form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <label for=\"message\">Message</label> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"message\" id=\"message\" cols=\"30\" rows=\"10\" class=\"form-control\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Say something about us\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Send Message\" class=\"btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t</div>
\t\t<div id=\"fh5co-counter\" class=\"fh5co-counters\" style=\"background-color: #3e64ef38;\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t<h2>Achievements</h2>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-4 text-center\">
\t\t\t\t\t\t<span id=\"icons\" class=\"icon-tv\"></span>
\t\t\t\t\t\t<spa>
\t\t\t\t\t\t\t<span class=\"fh5co-counter-label mt-40 animate-box\">A school website</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4 text-center\">
\t\t\t\t\t\t<span id=\"icons\" class=\"icon-v-card\"></span>
\t\t\t\t\t\t<span class=\"fh5co-counter-label mt-40 animate-box\">Generate E-Report Cards for students</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-4 text-center\">
\t\t\t\t\t\t<span id=\"icons\" class=\"icon-open-book\"></span>
\t\t\t\t\t\t<span class=\"fh5co-counter-label mt-40 animate-box\">A digitized library</span>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t<div id=\"fh5co-testimonial\" class=\"fh5co-bg-section\" style=\"background: #efefef;\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t<h2>Recent Post</h2>
\t\t\t\t\t<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab
\t\t\t\t\t\taliquam dolor eius.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t{% set posts = blogPosts.posts %}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t{% for post in posts %}
\t\t\t\t\t\t\t\t\t{% if loop.index < 5 %}
\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"fh5co-blog animate-box\">
\t\t\t\t\t\t\t\t\t\t\t\t{% if post.featured_images.count %}         
\t\t\t\t\t\t\t\t\t\t\t\t                {% for image in post.featured_images %}
\t\t\t\t\t\t\t\t\t\t\t\t                <a href=\"{{ post.url }}\"><img class=\"img-responsive\" data-src=\"{{ image.filename }}\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\tsrc=\"{{ image.thumb(350,247, {'mode': 'crop'}) }}\"                     alt=\"{{ image.description }}\"></a>
\t\t\t\t\t\t\t\t\t\t\t\t                {% endfor %}         
\t\t\t\t\t\t\t\t\t\t\t\t {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"blog-text blog-text-home\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h3><a href=\"{{ post.url }}\">{{ post.title }}</a></h3>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"posted_on\">{{ post.published_at|date('M d, Y') }}</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"posted_on\">Posted
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% if post.categories.count %} in {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for category in post.categories %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ category.url }}\">{{ category.name }}</a>{% if not loop.last %}, {% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>{{ html_limit(post.summary|raw, 120) }}</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ post.url }}\" class=\"btn btn-primary\">Read More</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"fh5co-blog\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t<h2>Gallery</h2>
\t\t\t\t\t<p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t\t<div class=\"container-fluid proj-bottom\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t{% set records = gallery.records %}
\t\t\t\t\t{% set displayColumn = gallery.displayColumn %}
\t\t\t\t\t{% set noRecordsMessage = gallery.noRecordsMessage %}
\t\t\t\t\t{% set detailsPage = gallery.detailsPage %}
\t\t\t\t\t{% set detailsKeyColumn = gallery.detailsKeyColumn %}
\t\t\t\t\t{% set detailsUrlParameter = gallery.detailsUrlParameter %}

\t\t\t\t\t{% for record in records %}

\t\t\t\t\t{% for image in record.gallery %}

\t\t\t\t\t{% if loop.index < 9 %}

\t\t\t\t\t\t<div class=\"col-md-4 col-sm-6 col-lg-3 fh5co-project animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t<img src=\"{{ image.thumb(392,261,{'mode':'crop'}) }}\" alt=\"Free HTML5 Website Template by FreeHTML5.co\"
\t\t\t\t\t\t\t\t\tclass=\"img-responsive\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>

\t\t\t\t\t{% endif %}

\t\t\t\t\t{% endfor %}

\t\t\t\t\t{% endfor %}

\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12 text-center animate-box\">
\t\t\t\t\t\t<p><a class=\"btn btn-primary btn-lg btn-learn\" href=\"{{ 'gallery'|page }}\">View More</a></p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t</div>

\t<div id=\"fh5co-started\" style=\"background-image:url(images/schLocations.jpg);\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t<h2>Get Started with SJSSS</h2>
\t\t\t\t\t<p>The St. Joseph’s Experience is very powerful</p>
\t\t\t\t\t<p>It gives students a unique sense of boldness. <br>
\t\t\t\t\t\tIt helps them to develop their God given talents and potentials. <br>
\t\t\t\t\t\tIt gives the girls the opportunity to explore different disciplines and integrate fields of knowledge,
\t\t\t\t\t\tand, to ask the right questions. <br>
\t\t\t\t\t\tHere students are uniquely prepared to not only fit in but to excel wherever
\t\t\t\t\t\tthey may find themselves.
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row animate-box\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<p><a href=\"{{ 'contact'|page }}\" class=\"btn btn-default btn-lg\">Contact Us</a></p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/home.htm", "");
    }
}
