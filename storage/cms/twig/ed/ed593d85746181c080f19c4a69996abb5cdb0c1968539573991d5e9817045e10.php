<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/themes/st-josephs/pages/events.htm */
class __TwigTemplate_a649c2eb9eb254f2f5236b2850fb46720189a8d5bf2730f74660bc74bafd84fd extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\"
\t\tstyle=\"background-image:url(";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/picture.jpg");
        echo ");\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Our Events and Activities</h1>
\t\t\t\t\t\t\t<h2>Over the years we have made so much progress in getting to see the potential of our ladies curtisy of the school events organized.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>
\t
\t";
        // line 18
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 18);
        // line 19
        echo "\t";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 19);
        // line 20
        echo "\t";
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 20);
        // line 21
        echo "\t";
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 21);
        // line 22
        echo "\t";
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 22);
        // line 23
        echo "\t";
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 23);
        // line 24
        echo "
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"pricing mt-30\">
\t\t\t\t\t";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 29
            echo "\t\t\t\t\t\t";
            if ((twig_date_converter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "date", [], "any", false, false, false, 29)) > twig_date_converter($this->env, twig_date_format_filter($this->env, "now")))) {
                // line 30
                echo "\t\t\t\t\t\t ";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 30) < 2)) {
                    // line 31
                    echo "\t\t\t\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t\t\t\t<h2>Upcoming Events</h2>
\t\t\t\t\t\t\t\t\t<p>";
                    // line 34
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 34), "html", null, true);
                    echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-6 text-center animate-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive abt-img pb-40\" src=\"";
                    // line 38
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "event_img", [], "any", false, false, false, 38), "thumb", [0 => 555, 1 => 450, 2 => ["mode" => "crop"]], "method", false, false, false, 38), "html", null, true);
                    echo "\" alt=\"work\"
\t\t\t\t\t\t\t\t\tstyle=\"margin-left: 50%;\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t    ";
                }
                // line 42
                echo "\t\t\t\t\t\t";
            }
            // line 43
            echo "\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t\t\t
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t\t<h2>Previous Events</h2>
\t\t\t\t\t\t<p>Our girls have always made us proud no matter how difficult the situation might be.</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"container-fluid proj-bottom container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 58
            echo "\t\t\t\t\t\t";
            if ((twig_date_converter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "date", [], "any", false, false, false, 58)) < twig_date_converter($this->env, twig_date_format_filter($this->env, "now")))) {
                // line 59
                echo "\t\t\t\t\t\t\t<div class=\"col-md-4 col-sm-6 fh5co-project animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"";
                // line 60
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "event_img", [], "any", false, false, false, 60), "path", [], "any", false, false, false, 60), "html", null, true);
                echo "\"
\t\t\t\t\t\t\t\t\t\talt=\"Free HTML5 Website Template by FreeHTML5.co\" class=\"img-responsive\">
\t\t\t\t\t\t\t\t\t<h3>";
                // line 62
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "name", [], "any", false, false, false, 62), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t\t\t\t<span>";
                // line 63
                echo call_user_func_array($this->env->getFunction('html_limit')->getCallable(), ["limit", twig_get_attribute($this->env, $this->source, $context["record"], "description", [], "any", false, false, false, 63), 25]);
                echo "</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 67
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "\t\t\t\t</div>
\t\t\t</div>

\t</div>";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/events.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 68,  180 => 67,  173 => 63,  169 => 62,  164 => 60,  161 => 59,  158 => 58,  154 => 57,  139 => 44,  125 => 43,  122 => 42,  115 => 38,  108 => 34,  103 => 31,  100 => 30,  97 => 29,  80 => 28,  74 => 24,  71 => 23,  68 => 22,  65 => 21,  62 => 20,  59 => 19,  57 => 18,  38 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"fh5co-header\" class=\"fh5co-cover fh5co-cover-sm\" role=\"banner\"
\t\tstyle=\"background-image:url({{ 'assets/images/picture.jpg'|theme }});\" data-stellar-background-ratio=\"0.5\">
\t\t<div class=\"overlay\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center\">
\t\t\t\t\t<div class=\"display-t\">
\t\t\t\t\t\t<div class=\"display-tc animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t<h1>Our Events and Activities</h1>
\t\t\t\t\t\t\t<h2>Over the years we have made so much progress in getting to see the potential of our ladies curtisy of the school events organized.</a></h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</header>
\t
\t{% set records = builderList.records %}
\t{% set displayColumn = builderList.displayColumn %}
\t{% set noRecordsMessage = builderList.noRecordsMessage %}
\t{% set detailsPage = builderList.detailsPage %}
\t{% set detailsKeyColumn = builderList.detailsKeyColumn %}
\t{% set detailsUrlParameter = builderList.detailsUrlParameter %}

\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"pricing mt-30\">
\t\t\t\t\t{% for record in records %}
\t\t\t\t\t\t{% if date(record.date) > date(\"now\"|date()) %}
\t\t\t\t\t\t {% if loop.index < 2 %}
\t\t\t\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t\t\t\t<div class=\"col-md-6 col-md-offset-3 text-center fh5co-heading\">
\t\t\t\t\t\t\t\t\t<h2>Upcoming Events</h2>
\t\t\t\t\t\t\t\t\t<p>{{ record.name }}</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-6 text-center animate-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive abt-img pb-40\" src=\"{{ record.event_img.thumb (555, 450, {'mode':'crop'}) }}\" alt=\"work\"
\t\t\t\t\t\t\t\t\tstyle=\"margin-left: 50%;\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t{% endfor %}
\t\t\t\t</div>
\t\t\t</div>
\t\t\t\t
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row animate-box\">
\t\t\t\t\t<div class=\"col-md-8 col-md-offset-2 text-center fh5co-heading\">
\t\t\t\t\t\t<h2>Previous Events</h2>
\t\t\t\t\t\t<p>Our girls have always made us proud no matter how difficult the situation might be.</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"container-fluid proj-bottom container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t{% for record in records %}
\t\t\t\t\t\t{% if date(record.date) < date(\"now\"|date()) %}
\t\t\t\t\t\t\t<div class=\"col-md-4 col-sm-6 fh5co-project animate-box\" data-animate-effect=\"fadeIn\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"{{ record.event_img.path }}\"
\t\t\t\t\t\t\t\t\t\talt=\"Free HTML5 Website Template by FreeHTML5.co\" class=\"img-responsive\">
\t\t\t\t\t\t\t\t\t<h3>{{ record.name }}</h3>
\t\t\t\t\t\t\t\t\t<span>{{ html_limit(record.description|raw, 25) }}</span>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t{% endfor %}
\t\t\t\t</div>
\t\t\t</div>

\t</div>", "/Users/user/Sites/stjosephsss/themes/st-josephs/pages/events.htm", "");
    }
}
