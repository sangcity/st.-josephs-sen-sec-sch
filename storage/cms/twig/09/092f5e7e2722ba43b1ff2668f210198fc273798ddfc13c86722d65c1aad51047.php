<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/user/Sites/stjosephsss/plugins/madnh/disqus/components/disqus/default.htm */
class __TwigTemplate_f232a6097466c190e1f4fa9e58d1fb7cb3d8c91618e7c36f4d28ac16b14a61fd extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["disqus"] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "disqus", [], "any", false, false, false, 1);
        // line 2
        if (twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "enable", [], "any", false, false, false, 2)) {
            // line 3
            echo "    ";
            if ( !twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "sub_domain", [], "any", false, false, false, 3)) {
                // line 4
                echo "        <div class=\"alert alert-warning\" role=\"alert\">
            <strong>Warning!</strong> please config Disqus sub domain first!
        </div>
    ";
            } else {
                // line 8
                echo "        <div id=\"disqus_thread\"></div>
        <script>
            var disqus_config = function () {
                this.page.url = '";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "page_url", [], "any", false, false, false, 11), "html", null, true);
                echo "' || window.location.href;
                this.page.identifier = \"";
                // line 12
                echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "page_id_prefix", [], "any", false, false, false, 12)) ? ((twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "page_id_prefix", [], "any", false, false, false, 12) . "_")) : ("")) . twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "page_id", [], "any", false, false, false, 12)), "html", null, true);
                echo "\" || window.location.href;
            };

            (function () {
                var d = document, s = d.createElement('script');
                s.src = '//";
                // line 17
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "sub_domain", [], "any", false, false, false, 17), "html", null, true);
                echo ".disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>";
                // line 22
                echo twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "noscript_content", [], "any", false, false, false, 22);
                echo "</noscript>
    ";
            }
        } else {
            // line 25
            echo "    ";
            echo twig_get_attribute($this->env, $this->source, ($context["disqus"] ?? null), "disabled_content", [], "any", false, false, false, 25);
            echo "
";
        }
        // line 27
        echo "
";
    }

    public function getTemplateName()
    {
        return "/Users/user/Sites/stjosephsss/plugins/madnh/disqus/components/disqus/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 27,  79 => 25,  73 => 22,  65 => 17,  57 => 12,  53 => 11,  48 => 8,  42 => 4,  39 => 3,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set disqus = __SELF__.disqus %}
{% if disqus.enable %}
    {% if not disqus.sub_domain %}
        <div class=\"alert alert-warning\" role=\"alert\">
            <strong>Warning!</strong> please config Disqus sub domain first!
        </div>
    {% else %}
        <div id=\"disqus_thread\"></div>
        <script>
            var disqus_config = function () {
                this.page.url = '{{ disqus.page_url }}' || window.location.href;
                this.page.identifier = \"{{ (disqus.page_id_prefix ? disqus.page_id_prefix ~ '_' : '') ~ disqus.page_id }}\" || window.location.href;
            };

            (function () {
                var d = document, s = d.createElement('script');
                s.src = '//{{ disqus.sub_domain }}.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>{{ disqus.noscript_content | raw }}</noscript>
    {% endif %}
{% else %}
    {{ disqus.disabled_content | raw }}
{% endif %}

", "/Users/user/Sites/stjosephsss/plugins/madnh/disqus/components/disqus/default.htm", "");
    }
}
